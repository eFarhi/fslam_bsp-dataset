function [A,b] = BuildLarge_A_and_b(fg,lfg,pose_index)
%BUILDLARGE_A_AND_B builds the Jacobian and RHS incrementaly to avoid issues caused by large FG
% Conventions:
% # A order is all landmarks by order of appearence and than all poses by index
% # first pose index is '1'
% # A is returned as a sparse matrix
%% ############ Scaffolding ####################
% fg = belief.getFactorsUnsafe;
% lfg = fg.linearize(DA.values);
% AA = lfg.augmentedJacobian;
% AA(:,end) = [];
% ##############################################
%% calc the column space of A
inv_var_num = lfg.keys.size;
inv_pose_num = pose_index;
inv_landmark_num = inv_var_num - inv_pose_num;
A_column_space = inv_landmark_num*3 + inv_pose_num*6;
%% Initialization
A = sparse(zeros(1,A_column_space));
b = sparse([]);
A_row_index_pointer = 1;
x_length = 6;
l_length = 3;
l_counter = 0;
l_index_vec = [];
%% Build A & b incrementaly Per Factor
for fac_index = 0 : fg.size-1
    % extract a factor from FG
    fac = fg.at(fac_index);
    % get the keys of involved variables
    fac_keys = fac.keys;
    % get the equivalent A & b
    lfac = lfg.at(fac_index);
    A_fac = lfac.augmentedJacobian;
    b_fac = A_fac(:,end);
    A_fac(:,end) = [];
    inc_row = length(b_fac);
    % b one fac at a time
    b(A_row_index_pointer:A_row_index_pointer+inc_row-1,1) = b_fac;
    % build A one fac at a time
    for inv_index = 0 : fac_keys.size-1
        inv_var.index = gtsam.symbolIndex(fac_keys.at(inv_index));
        inv_var.Char = gtsam.symbolChr(fac_keys.at(inv_index));
        switch inv_var.Char
            case double('x')
                A_column_start = inv_landmark_num*l_length + (inv_var.index-1)*x_length + 1;
                A_column_end = inv_landmark_num*l_length + inv_var.index*x_length;
                
                A(A_row_index_pointer:A_row_index_pointer+inc_row-1,A_column_start:A_column_end) ...
                    = A_fac(:,1:x_length);
                A_fac(:,1:x_length) = [];
                %                     switch class(fac) %use in case there is a new factor
                %                         case 'gtsam.BetweenFactorPose3'
                %                             A(A_row_index_pointer:A_row_index_pointer+inc_row-1,A_column_start:A_column_end) ...
                %                                 = A_fac(:,1:x_length);
                %                             A_fac(:,1:x_length) = [];
                %                         case 'gtsam.GenericProjectionFactorCal3_S2'
                %                             A(A_row_index_pointer:A_row_index_pointer+inc_row-1,A_column_start:A_column_end) ...
                %                                 = A_fac(:,1:x_length); %A_fac(:,end-x_length+1:end);
                %                             A_fac(:,1:x_length) = [];
                %                         case 'gtsam.PriorFactorPose3'
                %                             A(A_row_index_pointer:A_row_index_pointer+inc_row-1,A_column_start:A_column_end) ...
                %                                 = A_fac;
                %                         case 'gtsam.PriorFactorPoint3'
                %                             error('Hell just froze... go nuts !!')
                %                         otherwise
                %                             error(['add ',class(fac),' class as an option to BuildLarge_A_and_b()'])
                %                     end
            case double('l')
                % order by appearence
                if length(l_index_vec) < inv_var.index
                    l_counter = l_counter+1;
                    l_index_vec(inv_var.index) = l_counter;
                elseif l_index_vec(inv_var.index) == 0
                    l_counter = l_counter+1;
                    l_index_vec(inv_var.index) = l_counter;
                end
                l_index = l_index_vec(inv_var.index);
                A_column_start = l_length*(l_index -1) + 1;
                A_column_end = l_length*l_index;
                A(A_row_index_pointer:A_row_index_pointer+inc_row-1,A_column_start:A_column_end) ...
                    = A_fac(:,end-l_length+1:end);
                A_fac(:,end-l_length+1:end) = [];
            otherwise
                error(['define the size for ',char(inv_var.Char),' variable'])
        end
    end
    A_row_index_pointer = A_row_index_pointer + inc_row;
    
    % ########## scaffolding plot#############
    %         figure(3)
    %         subplot(1,2,1)
    %         spy(A)
    %         title('new')
    %         subplot(1,2,2)
    %         spy(AA(1:A_row_index_pointer-1,:))
    %         title('control')
    %     end
    %% For when using a different ordering (by index not order of appearence)
    %     A( :, ~any(A,1) ) = [];  % delete all zero columns
end

