%%
clear classes
clear all
clc
%%
[ DS ] = ibsp_ds09_LoadDataset( );

%%
n_frames = length(DS.GroundTruth.data);
fig = figure;
hold on
axis equal
for i = 1 : 1800%n_frames
    vec = DS.GroundTruth.data{i}(1:3,4);
    scatter3(vec(1),vec(2),vec(3),'k.')
end

%%
pose_index = 20;
GT_global_k1 = gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{pose_index}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{pose_index}(1:3,4)));
if pose_index > 1
    former_pose_index = pose_index -1;
    GT_global_k =  gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{former_pose_index}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{former_pose_index}(1:3,4)));
else
    GT_global_k = gtsam.Pose3(gtsam.Rot3(eye(3)),gtsam.Point3(zeros(3,1)));
end

a = GT_global_k1.between(GT_global_k);

figure
hold on
plot3([0 GT_global_k.translation.x],[0 GT_global_k.translation.y],[0 GT_global_k.translation.z],'b')
plot3([0 GT_global_k1.translation.x],[0 GT_global_k1.translation.y],[0 GT_global_k1.translation.z],'r--')
plot3([0 a.translation.x],[0 a.translation.y],[0 a.translation.z],'k-.')

scatter3(GT_global_k.translation.x,GT_global_k.translation.y,GT_global_k.translation.z,'ro')
scatter3(GT_global_k1.translation.x,GT_global_k1.translation.y,GT_global_k1.translation.z,'ko')
scatter3(a.translation.x,a.translation.y,a.translation.z,'b.')

%% 

n_frames = length(DS.GroundTruth.data);


figure(fig)
for i = 1:1800 %n_frames-1
    translation = DS.GroundTruth.data{i}(1:3,4);
    scatter3(translation(1),translation(2),translation(3))
    i
    pause(0.01);
end







%%

 GT_global_k1 = gtsam.Pose3(gtsam.Rot3(eye(3)),gtsam.Point3([2;1;0]));
    GT_global_k =  gtsam.Pose3(gtsam.Rot3(eye(3)),gtsam.Point3([1;0;0]));

    a = GT_global_k1.between(GT_global_k)

    b = GT_global_k.translation.vector - a.translation.vector
    
    
    
    
    