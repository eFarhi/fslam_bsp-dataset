function  ibsp_ds00_Main
%% IBSP-ds00_MAIN creates the information needed for planning
%
%
%% 
clear classes
clear all
close all
clc
%% Add paths
addpath(genpath('~/code/LBA/matlab/'));
addpath(genpath('~/code/0_COMMON_COMPUTATION'));
addpath(genpath('~/code/PlanningInTheGbs/'));
addpath(genpath('../../ibsp'));

%% Initializing Randomizer
stream = RandStream.getGlobalStream;
reset(stream);

%% Initializing Scenario

Scenario_name = 'Kitti'; %'Monitoring-Extended','Monitoring','Toy','MonitoringCircle', 'Oasis', 'Flower'
[FLAGS, Weights] = ibsp_ds01_PlanningConfigurationParams('iBSP');
FLAGS.Scenario_name = Scenario_name;

%% Results Storage
p_to_saving_results = '~/data/ibsp-ds';
designated_name = [FLAGS.Scenario_name '-' FLAGS.PlanningAlgorithm];
foldername = [];
timestamp = datestr(now,'yy-mm-dd-HH-MM-SS');
if isempty(designated_name)
    foldername = [foldername timestamp];
else
    foldername = [foldername designated_name '-' timestamp];
end
% foldername = [foldername '-' num2str(FLAGS.NumOfLookAheadSteps_max) 'steps'];

%% Prepare the Database 
ds_flags.loadRawImages = 0;
[DS] = ibsp_ds09_LoadDataset(ds_flags);

%% Generate Map

%% ML
FLAGS.MaximumLikelihoodMeasurements = 1;
if FLAGS.MaximumLikelihoodMeasurements
    warning('ML observations!!!');
end


%% Run Scenario
% ##########################
mean_pos_goal_all = [];
manuever_delta_vec = []; %DS.GroundTruth.trajectory(2:2500);
FLAGS.upload = 0; % 1- use upload file, 0 - start from scratch
just_isam_flag = 1;
FLAGS.onlyKnownVar = 0;
FLAGS.savestep4upload = 50;
% ##########################

if FLAGS.upload
    % enter the folder name in here, make sure it is inside the designated path
    foldername = 'RunFrom_900';
    % last saved pose_index
    FLAGS.pose_index = 1500; 
end

if just_isam_flag
    ibsp_ds02a_RunScenario(DS,FLAGS, Weights, mean_pos_goal_all, manuever_delta_vec, p_to_saving_results, foldername);
else
    ibsp_ds02_RunScenario(DS,FLAGS, Weights, mean_pos_goal_all, manuever_delta_vec, p_to_saving_results, foldername);
end
end