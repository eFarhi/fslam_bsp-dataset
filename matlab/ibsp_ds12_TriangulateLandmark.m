function [ DA,values,factors,keys,output] = ibsp_ds12_TriangulateLandmark( DA,index,current_pose,values,factors,current_payload,keys,RobotSettings)

switch current_payload
    case DA.OptionalPayloads.Mono_Camera
        %% init
        output.new_landmarks_num = 0;
        output.fac_of_new_landmarks = 0;
        %% Gather relevant measurements
        for feature_index = index'            
            state_index = find(DA.Payloads.Mono_Camera.Correspondence_Mat(feature_index,1:current_pose,2) == 1);
            measurements_index = DA.Payloads.Mono_Camera.Correspondence_Mat(feature_index,state_index,1);
            measurements = [];
            for state = 1:length(state_index)
                measurements(:,end+1) = DA.Payloads.Mono_Camera.data{state_index(state)}(:,measurements_index(state));
            end
            %% Pre Triangulation check
            pose_key_last = gtsam.symbol('x',current_pose);
            pose_last = values.all.at(pose_key_last);
            last = measurements(:,end);
            for i = 1 : length(state_index)-1
                pre = measurements(:,i);
                dist = norm(last(1:2)-pre(1:2));
                % get value for previous state for triangulation
                pose_key_pre = keys.pose_keys_vec(state_index(i)); %gtsam.symbol('x',state_index(i));
                pose_pre = DA.values.at(pose_key_pre);
                % Too close or too far
                dist_flag = 7 < dist && dist < 100;
                % two images are far enough
                dist_flag = dist_flag && norm(pose_last.between(pose_pre).translation.vector) > 0.01;
                if dist_flag
                    %% Triangulation
                    K = DA.Payloads.Mono_Camera.calib_mat;
                    last_pix = last(1:2);
                    pre_pix = pre(1:2);
                    landmark_init_value = triangulate_elad(pose_pre,pose_last, pre_pix,last_pix,K);
%                     landmark_init_value = triangulate_elad_old(pose_pre,pose_last, pre_pix,last_pix,K);

                    %% Post Triangulation check
%                     los = landmark_init_value.vector - pose_last.translation.vector;
                    l_pre = pose_pre.transform_to(landmark_init_value).vector;
                    l_last = pose_last.transform_to(landmark_init_value).vector;
                    % too close or too far
                    value_flag = 2 < norm(l_pre) && norm(l_pre) < 40 && 2 < norm(l_last) && norm(l_last) < 40;
                    % behind the camera
                    value_flag = value_flag && l_pre(end) > 0 && l_last(end) > 0 ;
                    % too much to the sides
%                     value_flag = value_flag && norm(los(1:2)) < 2 ;
                    if value_flag % all is well, we can use this value
                        %% Add Landmark value
                        l_key = gtsam.symbol('l',feature_index);
                        values.new.insert(l_key, landmark_init_value);
                        values.all.insert(l_key, landmark_init_value);
                        keys.landmark_keys_vec(feature_index,1) = l_key;
                        proj_fac = gtsam.GenericProjectionFactorCal3_S2(gtsam.Point2(pre_pix), DA.Payloads.Mono_Camera.noise_model, pose_key_pre, l_key,K);
                        factors.new.push_back( proj_fac );
                        factors.all.push_back(proj_fac);
                        proj_fac = gtsam.GenericProjectionFactorCal3_S2(gtsam.Point2(last_pix), DA.Payloads.Mono_Camera.noise_model, pose_key_last, l_key,K);
                        factors.new.push_back( proj_fac );
                        factors.all.push_back( proj_fac );
                        DA.Payloads.Mono_Camera.Correspondence_Mat(feature_index,state_index([i end]),2) = [2 2]; % document that these are used for triangulation
                        output.fac_of_new_landmarks = output.fac_of_new_landmarks + 2;
                        %% Add landmark priors from triangulation
                        prior_fac = gtsam.PriorFactorPoint3(l_key,landmark_init_value,RobotSettings.initial_landmark_sigma );
                        factors.new.push_back(prior_fac);
                        factors.all.push_back(prior_fac);
                        output.fac_of_new_landmarks = output.fac_of_new_landmarks + 1;
                        %% add factors for measurements taken between last & pre including
                        for j = i+1 : length(state_index)-1
                           pixels = gtsam.Point2(measurements(1:2,j)); 
                           pose_key = keys.pose_keys_vec(state_index(j)); %gtsam.symbol('x',state_index(j));
                           proj_fac = gtsam.GenericProjectionFactorCal3_S2(pixels, DA.Payloads.Mono_Camera.noise_model, pose_key, l_key,K);
                           factors.new.push_back( proj_fac );
                           factors.all.push_back( proj_fac );
                           output.fac_of_new_landmarks = output.fac_of_new_landmarks + 1;
                           DA.Payloads.Mono_Camera.Correspondence_Mat(feature_index,state_index(j),2) = 3; % document that is used as factor
                        end 
                        %% triangulated, hence no need to continue with this landmark
                        output.new_landmarks_num = output.new_landmarks_num + 1; % simple counter
                        break
                    end
                end
            end
            
        end
        
        
        
    case DA.OptionalPayloads.Lidar
        
    case DA.OptionalPayloads.Stereo_Camera
        
    otherwise
        warning('No such payload is known by the Triangulator, no action has been taken')
end


end




% ########### diff triangulation function #################
%                     last_pix = K.calibrate(gtsam.Point2(last(1:2))).vector; 
%                     pre_pix = K.calibrate(gtsam.Point2(pre(1:2))).vector; 
                    % triangulation returns a value in pose_last cam frame
%                     landmark_init_value = lba.triangulateDLT(pose_pre,pose_last, pre_pix,last_pix);



