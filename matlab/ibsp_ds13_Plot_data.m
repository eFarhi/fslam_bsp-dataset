function [ varargout ] = ibsp_ds13_Plot_data( varargin ) % (fig_config,DA,pose_index,OutputData,choose_fig,DS,fig_handle)
%IBSP_DS13_PLOT_DATA 

%% Variable Input
counter = length(varargin);
first_case = 6;
sec_case = 7;
switch counter
    case first_case
         fig_config = varargin{1};
         DA = varargin{2};
         pose_index = varargin{3};
         OutputData = varargin{4};
         choose_fig = varargin{5}; % 0-all isam , 1-birdeye view isam, 2-cam view isam, 10-all isam+ibsp , 11-birdeye view isam+ibsp, 12-cam view isam+ibsp,
         DS = varargin{6};
         fig_handle.main = figure;
         fig_handle.estimate = [];
         fig_handle.landmarks = [];
         fig_handle.estimate_ibsp = [];
         fig_handle.landmarks_ibsp = [];
         fig_handle.landmark_tags = [];
         fig_handle.gt = [];
         fig_handle.cov = [];
         fig_handle.cov_ibsp = [];
         hold on
         axis equal
         axis([fig_config.map.Boundary.W_E fig_config.map.Boundary.S_N fig_config.map.Boundary.Down])
         set(gcf,'color','white');
         xlabel(fig_config.label.x);
         ylabel(fig_config.label.y);
         zlabel(fig_config.label.z);
    case sec_case
         fig_config = varargin{1};
         DA = varargin{2};
         pose_index = varargin{3};
         OutputData = varargin{4};
         choose_fig = varargin{5}; % 0-all , 1-birdeye view, 2-cam view
         DS = varargin{6};
         fig_handle = varargin{7};
         %% Plot results
         % plot GT
         if pose_index == 1
             GT_NED = [DS.GroundTruth.origin(1:3,4)' ;DS.GroundTruth.data{pose_index}(1:3,4)'];
         else
             GT_NED = [DS.GroundTruth.data{pose_index-1}(1:3,4)' ;DS.GroundTruth.data{pose_index}(1:3,4)'];
         end
         figure(fig_handle.main);
         hold on
         fig_handle.gt = plot3(GT_NED(:,1),GT_NED(:,3), -GT_NED(:,2) , '-', 'Color', fig_config.design.RobotPose_Col_true);
          set(fig_handle.gt,'MarkerSize',fig_config.design.Markersize,'LineWidth',fig_config.design.Linewidth);

         % plot estimation
         pose_NED = gtsam.utilities.extractPose3(OutputData.method{1}.estimate);
         pose_NED = pose_NED(:,10:12);
         delete(fig_handle.estimate)  
         figure(fig_handle.main);
         hold on
         fig_handle.estimate = plot3(pose_NED(:,1), pose_NED(:,3), -pose_NED(:,2), '--', 'Color', fig_config.design.RobotPose_Col_est); 
         set(fig_handle.estimate,'MarkerSize',fig_config.design.Markersize,'LineWidth',fig_config.design.Linewidth);
         
         % plot cov
         if fig_config.plot.plot_cov
             for i = 1:fig_config.plot.cov_delta:length(OutputData.method{1}.cov_robot_position)
                 cov = OutputData.method{1}.cov_robot_position{i};
                 cov_EN = [cov(2,2), cov(2,1); cov(1,2), cov(1,1)];
                 pos_NED_i = pose_NED(i,:);
                 %              pos_EN  = [pos_NED_i(2), pos_NED_i(1)];
                 pos_EN  = [pos_NED_i(1), pos_NED_i(3)];
                 figure(fig_handle.main)
                 try
                     delete(fig_handle.cov(i))
                 catch
                 end
                 fig_handle.cov(i) = covarianceEllipse_v(pos_EN, cov_EN, fig_config.design.RobotPose_Col_est,'-');
             end
         end
         % plot landmarks
         landmark_gf = gtsam.utilities.extractPoint3(OutputData.method{1}.estimate);
         delete(fig_handle.landmarks)  
         figure(fig_handle.main);
         hold on
         fig_handle.landmarks = plot3(landmark_gf(:,1), landmark_gf(:,3), -landmark_gf(:,2), fig_config.design.marker.landmark_isam, 'Color', fig_config.design.MappedLandmark); 
         set(fig_handle.landmarks,'MarkerSize',fig_config.design.Markersize,'LineWidth',fig_config.design.Linewidth);
         if fig_config.plot.landmark_tag
             delete(fig_handle.landmark_tags)
             tags = find(OutputData.keys.landmark_keys_vec > 0);
             fig_handle.landmark_tags = text(landmark_gf(:,1), landmark_gf(:,3), -landmark_gf(:,2),num2str(tags));
         end

         if choose_fig >= 10
            %% Print ibsp method
            % plot estimation
            pose_NED = gtsam.utilities.extractPose3(OutputData.method{2}.estimate);
            pose_NED = pose_NED(:,10:12);
            delete(fig_handle.estimate_ibsp)
            figure(fig_handle.main);
            hold on
            fig_handle.estimate_ibsp = plot3(pose_NED(:,1), pose_NED(:,3), -pose_NED(:,2), '-.', 'Color', fig_config.design.RobotPose_Col_est_ibsp);
            set(fig_handle.estimate_ibsp,'MarkerSize',fig_config.design.Markersize,'LineWidth',fig_config.design.Linewidth);
            
            % plot cov
            if fig_config.plot.plot_cov
                for i = 1:fig_config.plot.cov_delta:length(OutputData.method{2}.cov_robot_position)
                    cov = OutputData.method{2}.cov_robot_position{i};
                    cov_EN = [cov(2,2), cov(2,1); cov(1,2), cov(1,1)];
                    pos_NED_i = pose_NED(i,:);
                    %              pos_EN  = [pos_NED_i(2), pos_NED_i(1)];
                    pos_EN  = [pos_NED_i(1), pos_NED_i(3)];
                    figure(fig_handle.main)
                    try
                        delete(fig_handle.cov_ibsp(i))
                    catch
                    end
                    fig_handle.cov_ibsp(i) = covarianceEllipse_v(pos_EN, cov_EN, fig_config.design.RobotPose_Col_est_ibsp,'-');
                end
            end
            % plot landmarks
            landmark_gf = gtsam.utilities.extractPoint3(OutputData.method{2}.estimate);
            delete(fig_handle.landmarks_ibsp)
            figure(fig_handle.main);
            hold on
            fig_handle.landmarks_ibsp = plot3(landmark_gf(:,1), landmark_gf(:,3), -landmark_gf(:,2), fig_config.design.marker.landmark_ibsp, 'Color', fig_config.design.MappedLandmark);
            set(fig_handle.landmarks_ibsp,'MarkerSize',fig_config.design.Markersize,'LineWidth',fig_config.design.Linewidth);            
         end
    otherwise
        error('number of input variables dont match the function')
end
%% Variable Output
drawnow;
varargout{1} = fig_handle;

%% Store the fig
filename = sprintf('%05d_trajectory.fig',pose_index);
figure(fig_handle.main);
saveas(gcf, [fig_config.path.p_to_save_figs filename], 'fig');

pause(0.1);

end

function h = covarianceEllipse_v(x,P,color, line_style)
% covarianceEllipse: plot a Gaussian as an uncertainty ellipse
% Based on Maybeck Vol 1, page 366
% k=2.296 corresponds to 1 std, 68.26% of all probability
% k=11.82 corresponds to 3 std, 99.74% of all probability
%
% covarianceEllipse(x,P,color)
% it is assumed x and y are the first two components of state x

[e,s] = eig(P(1:2,1:2));
s1 = s(1,1);
s2 = s(2,2);
k = 2.296;
[ex,ey] = ellipse( sqrt(s1*k)*e(:,1), sqrt(s2*k)*e(:,2), x(1:2) );
h = line(ex,ey,'color',color,'linestyle',line_style);
end

function [x,y] = ellipse(a,b,c)
% ellipse: return the x and y coordinates for an ellipse
% [x,y] = ellipse(a,b,c);
% a, and b are the axes. c is the center

% global ellipse_x ellipse_y
% if ~exist('elipse_x')
    q =0:2*pi/25:2*pi;
    ellipse_x = cos(q);
    ellipse_y = sin(q);
% end

points = a*ellipse_x + b*ellipse_y;
x = c(1) + points(1,:);
y = c(2) + points(2,:);
end

