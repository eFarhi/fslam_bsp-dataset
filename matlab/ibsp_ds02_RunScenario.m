function ibsp_ds02_RunScenario(DS, FLAGS, Weights, mean_pos_goal_all, manuever_delta_vec, p_to_saving, foldername)
%%
%
% ########### timing rep number ############
rep_calc = 1;
% ##########################################

    
    %% Load robot settings
    RobotSettings = ibsp_ds03_RobotConfigurationParams(DS);
    R_B_to_C = RobotSettings.R_B_to_C;
    Pose_actual.Position_NED = RobotSettings.origin;
    Pose_actual.R_G_to_C     = RobotSettings.R_G_to_C;
    
    %% Create an iSAM2 object
    if strcmp(FLAGS.isam_optimization,'GaussNewton') %'GaussNewton'
        isam_optimization = gtsam.ISAM2GaussNewtonParams;
    else
        isam_optimization = gtsam.ISAM2DoglegParams;
    end
    
    % isam_optimization.setWildfireThreshold(0); % paper value => 0.001
    
    isam_params = gtsam.ISAM2Params;
    % isam_params.setRelinearizeThreshold(0); % default 0.1
    isam_params.setOptimizationParams(isam_optimization);
    isam_params.setFactorization('qr');
    isam_params.setRelinearizeSkip(1); % default 10
    isam_params.setEnableRelinearization(true); % default true
    isam_params.setEnablePartialRelinearizationCheck(false); % default false
%% upload & storage
if FLAGS.upload
    %% upload data from memory
    pose_index = FLAGS.pose_index +1;
    upload_counter = floor((pose_index-1)/FLAGS.savestep4upload);
    load([p_to_saving filesep foldername filesep 'DetailedOutput/Accuracy_',num2str(pose_index -1)]);
    load([p_to_saving filesep foldername filesep 'DetailedOutput/Timing_',num2str(pose_index -1)]);
    load([p_to_saving filesep foldername filesep 'DetailedOutput/Counting_',num2str(pose_index -1)]);
    load([p_to_saving filesep foldername filesep 'DetailedOutput/keys_',num2str(pose_index -1)]);
    txt_graph = importdata([p_to_saving filesep foldername filesep 'DetailedOutput/fg_',num2str(pose_index-1),'.txt']);
    txt_val = importdata([p_to_saving filesep foldername filesep 'DetailedOutput/val_',num2str(pose_index-1),'.txt']); 
    %% build the belief
    graph = gtsam.NonlinearFactorGraph;
    graph = graph.string_deserialize(txt_graph);
    
    vals = gtsam.Values;
    vals = vals.string_deserialize(txt_val);
    
    belief = gtsam.ISAM2(isam_params);
    belief.update(graph, vals);
    for up=1:3
        belief.update();
    end
    estimate = belief.calculateEstimate();
    %% build the DA matrix
    load([p_to_saving filesep foldername filesep 'DetailedOutput/DA',num2str(pose_index -1)]);
    DA = ibsp_DataAssociation;
    DA.Payloads = temp_DA_payloads;
    DA.values = gtsam.Values;
    DA.values.insert(vals);
    DA.Payloads.Mono_Camera.noise_model = RobotSettings.image_noise_model;
    DA.Payloads.Mono_Camera.calib_mat = DS.Payloads.Mono_Camera.calib_mat;
    clear temp_DA_payloads
    %% Load last known R E & d
    try
        load([p_to_saving filesep foldername filesep 'DetailedOutput/R',num2str(pose_index -1)]);
        load([p_to_saving filesep foldername filesep 'DetailedOutput/E',num2str(pose_index -1)]);
        load([p_to_saving filesep foldername filesep 'DetailedOutput/d',num2str(pose_index -1)]);
        InfUpdateOutput.results{3}.R_k1_k1 = upl_R;
        InfUpdateOutput.results{3}.E = upl_E;
        InfUpdateOutput.results{3}.d_k1_k1 = upl_d;
        clear upl_R upl_E upl_d
    catch
        disp('No stored R matrix to upload')
        InfUpdateOutput.results{3}.R_k1_k1 = [];
        InfUpdateOutput.results{3}.d_k1_k1 = [];
        InfUpdateOutput.results{3}.E = [];
    end
    %% more required settings 
    pose_curr_key = gtsam.symbol('x', pose_index-1);
    OutputData.method{1}.method_name = 'iSAM';
    manuever_index  = 1;
    fig_handle     = [];
    p_to_saving_results = [p_to_saving filesep foldername filesep];
    %% Visualization configurations
    fig_config = ibsp_ds05_PlotConfiguration(FLAGS,p_to_saving_results);
    %% Finish loading notice
    disp('## Finished Loading restoration point ##')
else
    %% Post-Calc Storage
    if exist([p_to_saving filesep foldername],'dir')
        error('Output folder already exists!')
    end
    mkdir(p_to_saving, foldername);
    p_to_saving_results = [p_to_saving filesep foldername filesep];
     %% Visualization configurations
    fig_config = ibsp_ds05_PlotConfiguration(FLAGS,p_to_saving_results);
    %% Set Prior & I.C
    belief = gtsam.ISAM2(isam_params);
    initial_pose = gtsam.Pose3(gtsam.Rot3(RobotSettings.origin.rot), gtsam.Point3(RobotSettings.origin.loc));
    
    DA = ibsp_DataAssociation;
    DA.Payloads.Mono_Camera.noise_model = RobotSettings.image_noise_model;
    DA.Payloads.Mono_Camera.calib_mat = DS.Payloads.Mono_Camera.calib_mat;
    
    newFactors.predicted = gtsam.NonlinearFactorGraph;
    newFactors.new = gtsam.NonlinearFactorGraph;
    newFactors.all = gtsam.NonlinearFactorGraph;
    
    newValues.predicted  = gtsam.Values;
    newValues.new  = gtsam.Values;
    newValues.all  = gtsam.Values;
    
    pose_curr_key = gtsam.symbol('x', 1);
    factor = gtsam.PriorFactorPose3(pose_curr_key, initial_pose, gtsam.noiseModel.Diagonal.Sigmas(RobotSettings.initial_pose_sigma'));
    
    newFactors.predicted.push_back(factor);
    newValues.predicted.insert(pose_curr_key, initial_pose);
    
    newFactors.all.push_back(factor);
    newValues.all.insert(pose_curr_key, initial_pose);
    
    keys.pose_keys_vec = pose_curr_key;
    % keys.landmark_keys_vec = [];
    %% first step Obs Model
    pose_index = 1;
    CurrentReading = ibsp_ds10_GetMeasurements(DS,pose_index,DS.OptionalPayloads.Mono_Camera);
    [DA,newValues,newFactors,keys] = ibsp_ds08_DataAssociation(DA,CurrentReading,pose_index,newValues,newFactors,keys);
    
    %% Initialize Manuever
    result   = belief.update(newFactors.all, newValues.all);
    estimate = belief.calculateEstimate();
    DA.values = estimate;
    Timing.compareFG_time = [];
    Timing.infupdate = [];
    
    %% Initialize LOG structure
    OutputData.method{1}.method_name = 'iSAM';
    % OutputData.method{2}.method_name = 'iBSP';
    pose_index = 1;
    method_index = 1;
    OutputData = ibsp_ds14_UpdateOutput(OutputData,method_index,belief,result,estimate,newValues.all,newFactors.all,pose_index,FLAGS,keys,manuever_delta_vec);
    
    % method_index = 2;
    % OutputData = ibsp_ds14_UpdateOutput(OutputData,method_index,belief,result,estimate,newValues,newFactors,pose_index,FLAGS,keys,manuever_delta_vec);
    
    pose_index      = 2;
    manuever_index  = 1;
    fig_handle     = [];
    upload_counter = floor((pose_index-1)/FLAGS.savestep4upload);
    
    InfUpdateOutput.results{3}.R_k1_k1 = [];
    InfUpdateOutput.results{3}.d_k1_k1 = [];
    InfUpdateOutput.results{3}.E = [];
    
end
%% Print Status
    if fig_config.plot.plot_flag
        fig_handle = ibsp_ds13_Plot_data(fig_config,DA,pose_index,OutputData,[0],DS);
        mov(1) = getframe;
    end
    if ~FLAGS.upload == exist([p_to_saving_results filesep 'DetailedOutput'],'dir')
       error('DetailedOutput folder is missing or already exists!');    
    elseif ~exist([p_to_saving_results filesep 'DetailedOutput'],'dir')
       mkdir(p_to_saving_results, 'DetailedOutput');
   end
%% Performing Passive Manuever
while ~FLAGS.completed_objectives
    pose_index
    %% Planning the next menuever
    if manuever_index > length(manuever_delta_vec)
        % commence planning
        [next_manuever,PlanningOutput,AdditionalOutput] = ibsp_ds06_Planning(DS,DA,keys,RobotSettings,FLAGS,pose_index,belief);
        if AdditionalOutput.manuever_length == 0
            % if there isn't next step, terminate
            FLAGS.completed_objectives = true;
            continue
        else
            % MPC
            manuever_delta_vec{end+1} = next_manuever{1};
            % not MPC - incomplete!!!! - need to make k+2|k located in manuever{2} into k+2|k+1 located in manuever{1}
            %             manuever_delta_vec{end+AdditionalOutput.manuever_length} = [];
            %             manuever_delta_vec(end-AdditionalOutput.manuever_length+1:end) = next_manuever;
        end
    end
    manuever_delta = manuever_delta_vec{manuever_index};
    count.fac_k1_k_size(pose_index) = PlanningOutput.newFactors_k1_k.size;
    %% Move the Robot
    pose_prev_key = pose_curr_key;
    pose_curr_key = gtsam.symbol('x', pose_index);
    keys.pose_keys_vec = [keys.pose_keys_vec; pose_curr_key];
    
    %% Motion Model
    % Create Factor Graph and Values to hold new measurements
    newFactors.predicted = gtsam.NonlinearFactorGraph;
    newFactors.new = gtsam.NonlinearFactorGraph;
    newFactors.all = gtsam.NonlinearFactorGraph;
    
    newValues.predicted  = gtsam.Values;
    newValues.new  = gtsam.Values;
    newValues.all  = gtsam.Values;
    
    [odometry] = ibsp_ds04_GetOdometry(DS,RobotSettings,pose_index,manuever_delta);
    
    odom_fac = gtsam.BetweenFactorPose3(pose_prev_key, pose_curr_key, odometry, RobotSettings.odometry_noise_model);
    newFactors.predicted.push_back(odom_fac);
    newFactors.all.push_back(odom_fac);
    pose_predict = estimate.at(pose_prev_key).compose(odometry);
    newValues.predicted.insert(pose_curr_key, pose_predict);
    newValues.all.insert(pose_curr_key, pose_predict);
    
    %% Obs Model
    %     CurrentReading = ibsp_ds10_GetMeasurements(DS,pose_index,DS.OptionalPayloads.Mono_Camera);
    Measurements = DS.Payloads.Mono_Camera.data{pose_index};
    Descriptor = DS.Payloads.Mono_Camera.desc{pose_index};
    CurrentReading = struct('Payload',DS.OptionalPayloads.Mono_Camera,'Measurements',Measurements,'Descriptor',Descriptor,'Correspondence',[]);
    CurrentReading.Correspondence =  DS.DA.Correspondence_mat(:,pose_index);
    [DA,newValues,newFactors,keys,DA_output] = ibsp_ds08_DataAssociation(DA,CurrentReading,pose_index,newValues,newFactors,keys,RobotSettings);
    
    count.existing_landmark_num(pose_index) = DA_output.existing_landmarks_num;
    count.new_landmarks(pose_index) = DA_output.new_landmarks_num;
    count.new_fac_of_new_landmarks(pose_index) = DA_output.fac_of_new_landmarks;
    count.fac_k1_k1_size(pose_index) = newFactors.all.size;
    %% JUST FOR KNOWN LANDMARKS - compare FG_k+1|k to FG_k+1|k+1 & fix DA
    if FLAGS.onlyKnownVar
        predicted_compare = ibsp_ds15_CompareFG(newFactors.predicted,PlanningOutput.newFactors_k1_k,rep_calc);%,newValues);
        compare.predicted = predicted_compare;
        Timing.predicted.compareFG_time(pose_index) = compare.predicted.compareFG_time;
        if ~compare.predicted.match
            % fix newFactors_k1_k1 to contain only new factors
            predicted_compare = ibsp_ds17_UpdateNewFG(compare.predicted,newFactors.predicted,newValues.predicted);
            compare.predicted = predicted_compare;
            % fix DA of belief_k1_k
            [predicted_PlanningOutput] = ibsp_ds18_FixDA(PlanningOutput,compare.predicted,isam_params,rep_calc);
            Timing.predicted.fixDA_time(pose_index) = predicted_PlanningOutput.fix_time;
            Timing.predicted.test_fixDA_time(pose_index) = predicted_PlanningOutput.test_fix_time;
            count.predicted.fac_removed_k1_k(pose_index) = predicted_PlanningOutput.fac_removed;
            count.predicted.fac_added_k1_k(pose_index) = predicted_PlanningOutput.fac_added;
        else
            predicted_PlanningOutput = PlanningOutput;
            predicted_PlanningOutput.add_new_values_k1_k1 = gtsam.Values;
            Timing.predicted.fixDA_time(pose_index) = 0;
            count.predicted.fac_removed_k1_k(pose_index) = 0;
            count.predicted.fac_added_k1_k(pose_index) = 0;
        end
        
        count.predicted.consistant_DA(pose_index) = compare.predicted.match;
        count.predicted.unchanged_fac_k1_k(pose_index) = count.fac_k1_k_size(pose_index)-count.predicted.fac_removed_k1_k(pose_index);
    else
        %% compare FG_k+1|k to FG_k+1|k+1 & fix DA
        compare = ibsp_ds15_CompareFG(newFactors.all,PlanningOutput.newFactors_k1_k,rep_calc);%,newValues);
        Timing.compareFG_time(pose_index) = compare.compareFG_time;
        
        if ~compare.match
            % fix newFactors_k1_k1 to contain only new factors
            compare = ibsp_ds17_UpdateNewFG(compare,newFactors.all,newValues.all);
            % fix DA of belief_k1_k
            [PlanningOutput] = ibsp_ds18_FixDA(PlanningOutput,compare,isam_params,rep_calc);
            Timing.fixDA_time(pose_index) = PlanningOutput.fix_time;
            count.fac_removed_k1_k(pose_index) = PlanningOutput.fac_removed;
            count.fac_added_k1_k(pose_index) = PlanningOutput.fac_added;
        else
            Timing.fixDA_time(pose_index) = 0;
            count.fac_removed_k1_k(pose_index) = 0;
            count.fac_added_k1_k(pose_index) = 0;
        end
        count.consistant_DA(pose_index) = compare.match;
        count.unchanged_fac_k1_k(pose_index) = count.fac_k1_k_size(pose_index)-count.fac_removed_k1_k(pose_index);
    end
     %% inference Update for timing
    if newFactors.predicted.size() > 0
        if FLAGS.onlyKnownVar
            % only factors related to existing variables
            [~,~,predicted_InfUpdateOutput] = ibsp_ds16_InferenceUpdate(isam_params,belief,newFactors.predicted,newValues.predicted,predicted_PlanningOutput,predicted_compare,rep_calc,InfUpdateOutput.results{3}.R_k1_k1,InfUpdateOutput.results{3}.E,InfUpdateOutput.results{3}.d_k1_k1,pose_index);
            [belief,ibsp_belief,InfUpdateOutput] = ibsp_ds16_InferenceUpdate(isam_params,belief,newFactors.all,newValues.all,[],[],rep_calc,InfUpdateOutput.results{3}.R_k1_k1,InfUpdateOutput.results{3}.E,InfUpdateOutput.results{3}.d_k1_k1,pose_index);
            Timing.predicted.infupdate(pose_index,:) = predicted_InfUpdateOutput.timing_mat;
%             belief.update(newFactors.all,newValues.all);
            InfUpdateOutput.predicted = predicted_InfUpdateOutput;
            
        else
            % all factors
            [belief,ibsp_belief,InfUpdateOutput] = ibsp_ds16_InferenceUpdate(isam_params,belief,newFactors.all,newValues.all,PlanningOutput,compare,rep_calc,InfUpdateOutput.results{3}.R_k1_k1,InfUpdateOutput.results{3}.E,InfUpdateOutput.results{3}.d_k1_k1,pose_index);
            Timing.infupdate(pose_index,:) = InfUpdateOutput.timing_mat;
        end
        %% accuracy aspects
        isam_val = belief.calculateBestEstimate();
        if FLAGS.onlyKnownVar
            ibsp_val = isam_val;
        else
            ibsp_val = ibsp_belief.calculateBestEstimate();
        end
        isam_pose = [];
        ibsp_pose = [];
        for val = 1:pose_index
            isam_pose{val} = isam_val.at(keys.pose_keys_vec(val)).matrix;
            ibsp_pose{val} = ibsp_val.at(keys.pose_keys_vec(val)).matrix;
        end
        index = find(keys.landmark_keys_vec > 0);
        isam_landmark = [];
        ibsp_landmark = [];
        for val = index'
            isam_landmark(:,end+1) = [val ; isam_val.at(keys.landmark_keys_vec(val)).vector];
            ibsp_landmark(:,end+1) = [val ; ibsp_val.at(keys.landmark_keys_vec(val)).vector];
        end
        accuracy{pose_index}.isam_pose = isam_pose;
        accuracy{pose_index}.ibsp_pose = ibsp_pose;
        accuracy{pose_index}.isam_landmark = isam_landmark;
        accuracy{pose_index}.ibsp_landmark = ibsp_landmark;
        %% further update upto convergence
        %         % init timing parameters
        %         update_iter_num = 10;
        %         isam_up_time = zeros(update_iter_num,1);
        %         ibsp_up_time = zeros(update_iter_num,1);
        %
        %         for rep = 1:rep_calc
        %             % init convergence iter counter
        %             isam_counter = 1;
        %             ibsp_counter = 1;
        %             % init convergence conditions
        %             isam_error_before = InfUpdateOutput.error.isam_error_before;
        %             ibsp_error_before = InfUpdateOutput.error.ibsp_error_before;
        %
        %             isam_error_after = InfUpdateOutput.error.isam_error_after;
        %             ibsp_error_after = InfUpdateOutput.error.ibsp_error_after;
        %
        %             % update isam untill convergence
        %             isam_result = InfUpdateOutput.results{5}.result_k1_k1;
        %             belief_temp = gtsam.ISAM2(belief);
        %             while 1
        %                 if  abs(isam_error_after - isam_error_before) > 0.5
        %                     tic
        %                     isam_result = belief_temp.update();
        %                     isam_up_time(isam_counter) = isam_up_time(isam_counter) + toc;
        %                     isam_error_before = isam_error_after;
        %                     isam_error_after = belief_temp.getFactorsUnsafe.error(belief_temp.calculateBestEstimate);
        %                     InfUpdateOutput.error.isam_error_before(isam_counter+1) = isam_error_before;
        %                     InfUpdateOutput.error.isam_error_after(isam_counter+1) = isam_error_after;
        %                 else
        %                     break
        %                 end
        %                 isam_counter = isam_counter + 1 ;
        %                 if isam_counter > update_iter_num
        %                     isam_counter = isam_counter - 1;
        %                     break
        %                 end
        %             end
        %             Timing.update(1:isam_counter,1,pose_index) =  isam_up_time(1:isam_counter)./rep_calc;
        %             % update ibsp untill convergence
        %             ibsp_result = InfUpdateOutput.ibsp_result_k1_k1;
        %             ibsp_belief_temp = gtsam.ISAM2(ibsp_belief);
        %             while 1
        %                 if  abs(ibsp_error_after - ibsp_error_before) > 0.5
        %                     tic
        %                     ibsp_result = ibsp_belief_temp.update();
        %                     ibsp_up_time(ibsp_counter) =  ibsp_up_time(ibsp_counter) + toc;
        %                     ibsp_error_before = isam_error_after;
        %                     ibsp_error_after = ibsp_belief_temp.getFactorsUnsafe.error(ibsp_belief_temp.calculateBestEstimate);
        %                     InfUpdateOutput.error.ibsp_error_before(ibsp_counter+1) = ibsp_error_before;
        %                     InfUpdateOutput.error.ibsp_error_after(ibsp_counter+1) = ibsp_error_after;
        %                 else
        %                     break
        %                 end
        %                 ibsp_counter = ibsp_counter + 1 ;
        %                 if ibsp_counter > update_iter_num
        %                     ibsp_counter = ibsp_counter - 1;
        %                     break
        %                 end
        %             end
        %             Timing.update(1:ibsp_counter,2,pose_index) =  ibsp_up_time(1:ibsp_counter)./rep_calc;
        %         end
        
        %% updating inference 2nd update till convergence
        %         isam_counter = 1;
        %         result = InfUpdateOutput.results{5}.result_k1_k1;
        %         while 1
        %             if  abs(isam_error_after - isam_error_before) > 0.5
        %                 result = belief.update();
        %             else
        %                 break
        %             end
        %             isam_counter = isam_counter + 1 ;
        %             if isam_counter > update_iter_num
        %                 isam_counter = isam_counter - 1;
        %                 break
        %             end
        %         end
        %         ibsp_counter = 1;
        %         ibsp_result = InfUpdateOutput.ibsp_result_k1_k1;
        %         while 1
        %             if  abs(ibsp_error_after - ibsp_error_before) > 0.5
        %                 ibsp_result = ibsp_belief.update();
        %             else
        %                 break
        %             end
        %             ibsp_counter = ibsp_counter + 1 ;
        %             if ibsp_counter > update_iter_num
        %                 ibsp_counter = ibsp_counter - 1;
        %                 break
        %             end
        %         end
        %         estimate = belief.calculateBestEstimate();
        %         DA.values = estimate;
        %         ibsp_estimate = ibsp_belief.calculateBestEstimate();
        for i = 1:5
            result = belief.update();
        end
        estimate = belief.calculateBestEstimate();
        DA.values = estimate;
    end
    
    %% Save 2 Output
    
    method_index = 1;
    OutputData = ibsp_ds14_UpdateOutput(OutputData,method_index,belief, result,estimate,newValues.all,newFactors.all,pose_index,FLAGS,keys,manuever_delta_vec);
    
    %     method_index = 2;
    %     OutputData = ibsp_ds14_UpdateOutput(OutputData,method_index,ibsp_belief, ibsp_result,ibsp_estimate,newValues,newFactors,pose_index,FLAGS,keys,manuever_delta_vec);
    
    % Update figure
    if fig_config.plot.plot_flag
        fig_handle = ibsp_ds13_Plot_data(fig_config,DA,pose_index,OutputData,[0],DS,fig_handle);
        %         mov(pose_index+1) = getframe; % for creating a movie
    end
    %% Prep 4 Next Step
    manuever_index = manuever_index +1;
    pose_index = pose_index + 1;
    AdditionalOutput.curr_pose = [];
    AdditionalOutput.manuever = [];
% %     save([p_to_saving_results '/OutputData_' num2str(pose_index-1)], 'OutputData');
% %     save([p_to_saving_results 'DetailedOutput/compare_' num2str(pose_index-1)], 'compare');
% %     save([p_to_saving_results 'DetailedOutput/AdditionalOutput_' num2str(pose_index-1)], 'AdditionalOutput');
% %     save([p_to_saving_results 'DetailedOutput/InfUpdateOutput_' num2str(pose_index-1)], 'InfUpdateOutput');
    save([p_to_saving_results 'DetailedOutput/Accuracy_' num2str(pose_index-1)], 'accuracy');
    save([p_to_saving_results 'DetailedOutput/Timing_' num2str(pose_index-1)], 'Timing');
    save([p_to_saving_results 'DetailedOutput/Counting_' num2str(pose_index-1)], 'count');
    
    if upload_counter < floor((pose_index-1)/FLAGS.savestep4upload)
        upload_counter = floor((pose_index-1)/FLAGS.savestep4upload);
        % save the belief
        save([p_to_saving filesep foldername filesep 'DetailedOutput/keys_',num2str(pose_index -1)],'keys');
        graph = belief.getFactorsUnsafe;
        vals = belief.calculateBestEstimate;
        txt_graph = graph.string_serialize;
        txt_vals = vals.string_serialize;
        save([p_to_saving filesep foldername filesep 'DetailedOutput/fg_',num2str(pose_index-1),'.txt'],'txt_graph');
        save([p_to_saving filesep foldername filesep 'DetailedOutput/val_',num2str(pose_index-1),'.txt'],'txt_vals');
        % save DA matrix
        temp_DA_payloads = DA.Payloads;
        save([p_to_saving filesep foldername filesep 'DetailedOutput/DA',num2str(pose_index -1)],'temp_DA_payloads');
        % save R E & d
        upl_R = InfUpdateOutput.results{3}.R_k1_k1;
        upl_E = InfUpdateOutput.results{3}.E;
        upl_d = InfUpdateOutput.results{3}.d_k1_k1;
        save([p_to_saving filesep foldername filesep 'DetailedOutput/R',num2str(pose_index -1)],'upl_R','-v7.3');
        save([p_to_saving filesep foldername filesep 'DetailedOutput/E',num2str(pose_index -1)],'upl_E','-v7.3');
        save([p_to_saving filesep foldername filesep 'DetailedOutput/d',num2str(pose_index -1)],'upl_d','-v7.3');
    end
    
end

%% save output file
OutputData.last_pose_index = pose_index-1;
save([p_to_saving_results 'OutputData'], 'OutputData');


end

