% clear all
% clc
%%
% mat_size = 1000000;
% var_num = 32000;
% fac_num = floor(mat_size/6);
% var_indx = mod(1:fac_num,var_num);
% var_indx(var_indx==0) = var_num;
% fg = gtsam.NonlinearFactorGraph;
% val = gtsam.Values;
% for i = 1:fac_num
%     fg.add(gtsam.PriorFactorPose3(gtsam.symbol('x',var_indx(i)),gtsam.Pose3,gtsam.noiseModel.Diagonal.Sigmas([50 50 50 70 70 70]')));
%     try
%         val.insert(gtsam.symbol('x',var_indx(i)),gtsam.Pose3);
%     catch
%     end
% end
% lfg = fg.linearize(val);
% [A,b] = BuildLarge_A_and_b(fg,lfg,fac_num);
% 
% %%
% delta_T = [mvnrnd(zeros(3,3),1e-15^2*eye(3)) mvnrnd(zeros(1,3),1e-15^2*ones(1,3))'; 0 0 0 1];
% delta_L = mvnrnd(zeros(1,3),1e-15^2*ones(1,3))';

%% breaking down qr
matsize = [2200 2000];
A = sparse(rand(matsize(1),matsize(2)));
b = A(:,end);
[r,d] = infup010_HouseHolder(A,b);

smatNum = min(2,matsize(2));
smatsize = [matsize(1) floor(matsize(2)/smatNum)];
last = 0;
for i = 1:smatNum-1
   a{i} = zeros(matsize);
   a{i}(:,last+1:last+smatsize(2)) = A(:,last+1:last+smatsize(2));
   last = last+smatsize(2);
   [qtemp,rtemp,etemp] = qr(a{i});
%    q{i} = zeros(matsize(1),matsize(1));
%    r{i} = zeros(matsize);
%    e{i} = zeros(matsize);

   q{i} = qtemp;
   r{i} = rtemp;
   e{i} = etemp;
end
a{smatNum} = zeros(matsize);
a{smatNum}(:,last+1:end) = A(:,last+1:end);
[qtemp,rtemp,etemp] = qr(a{smatNum});
% q{smatNum} = zeros(matsize(1),matsize(1));
% r{smatNum} = zeros(matsize);
% e{smatNum} = zeros(matsize);
q{smatNum} = qtemp;
r{smatNum} = rtemp;
e{smatNum} = etemp;
   
recR = r{1};
recE = e{1};
recQ = q{1};
for i = 2:smatNum
    recR = recR + r{i};
    recE = recE + e{i};
    recQ = recQ + q{i};
end
% [Q,R] = qr(A);







