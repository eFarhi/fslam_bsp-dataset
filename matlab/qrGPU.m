function [Q,R,E] = qrGPU(A)
%QRGPU preforms qr factorization
[Q,R,E] = qr(A);
end

