classdef ibsp_DataAssociation
    %DATAASSOCIATION deffines a DA class, holding a structure for each
    %payload type
    
    properties
        OptionalPayloads = struct('Mono_Camera','Mono_Camera','Lidar','Lidar',...
            'Stereo_Camera','Stereo_Camera'...
            )
        Payloads = struct(...
            'Mono_Camera',struct('Correspondence_Mat',[],'Descriptors',[],'data_index',[],'data',[],'noise_model',[],'calib_mat',[]),...
            'Lidar',struct('Correspondence_Mat',[],'data_index',[],'data',[],'noise_model',[])...
            ,'Stereo_Camera',struct('Correspondence_Mat',[],'Descriptors',[],'data_index',[],'data',[],'noise_model',[],'calib_mat',[])...
            )
        time_index = []
        values = gtsam.Values;
    end
    
    methods
    end
    
end

