function [ DS ] = ibsp_ds09_LoadDataset(flag)
%IBSP_DS09_LOADDATASET builds the dataset variable

%% Create an empty dataset varaible
DS = ibsp_dataset;
max_index = 2500;
%% Try to use pre-calculated DS file
% try
%     load([DS.dataset_path 'DS.mat']);
%     disp('## loaded pre-calculated Dataset file ##')
%     return
% catch
% end

%% Load dataset
ds_name = dir([DS.dataset_path '*.mat']);
ds = load([DS.dataset_path ds_name.name]);
data = ds.data;

%% upload Robot information

%% upload Payloads
DS.Payloads.Mono_Camera.calib_mat = gtsam.Cal3_S2...
    (data.Calibration.fx* data.Settings.scale,...
    data.Calibration.fy* data.Settings.scale,...
    data.Calibration.skew...
    ,data.Calibration.c_x* data.Settings.scale,...
    data.Calibration.c_y* data.Settings.scale);
DS.Payloads.Mono_Camera.Path = 'Dataset/raw';
DS.Payloads.Lidar.Path = '';
DS.Payloads.Stereo_Camera.Path = '';
DS.Payloads.IMU.Path = '';
%% Get payload Calibration parameters
% DS.Payloads.Mono_Camera.R_B_to_C = [7.967514e-03 -9.999679e-01 -8.462264e-04;...
%     -2.771053e-03 8.241710e-04 -9.999958e-01;...
%     9.999644e-01 7.969825e-03 -2.764397e-03];
% 
% DS.Payloads.Mono_Camera.t_C_to_B_C = [-1.377769e-02 -5.542117e-02 -2.918589e-01]';

DS.Payloads.Mono_Camera.R_B_to_C = [0 -1 0;...
    0 0 -1;...
    1 0 0];

DS.Payloads.Mono_Camera.t_C_to_B_C = [0 0 0]';

%% upload Ground Truth
% init pose
pose     = [];
Tr_0_inv = [];
max_index = min (max_index, size(dir([DS.GroundTruth.Path '/*.txt']),1) - 1);
% for all oxts packets do
defname = '0000000000';
for pose_index= 1 : max_index
    % Load oxts file of pose_index
    oxts_name = [defname(1:end- length(num2str(pose_index))) num2str(pose_index)];
    file_name = [DS.GroundTruth.Path '/' oxts_name '.txt'];
    oxts = dlmread(file_name);
    % if there is no data => no pose
  if isempty(oxts)
    pose{pose_index} = [];
    continue;
  end

  if pose_index ==1
      scale = cos(oxts(1) * pi / 180.0);
  end
  
  % translation vector
  er = 6378137;
  t(1,1) = scale * oxts(2) * pi * er / 180;
  t(2,1) = scale * er * log( tan((90+oxts(1)) * pi / 360) );
  t(3,1) = oxts(3);

  % rotation matrix (OXTS RT3000 user manual, page 71/92)
  rx = oxts(4); % roll
  ry = oxts(5); % pitch
  rz = oxts(6); % heading 
  Rx = [1 0 0; 0 cos(rx) -sin(rx); 0 sin(rx) cos(rx)]; % base => nav  (level oxts => rotated oxts)
  Ry = [cos(ry) 0 sin(ry); 0 1 0; -sin(ry) 0 cos(ry)]; % base => nav  (level oxts => rotated oxts)
  Rz = [cos(rz) -sin(rz) 0; sin(rz) cos(rz) 0; 0 0 1]; % base => nav  (level oxts => rotated oxts)
  R  = Rz*Ry*Rx;
  
  % normalize translation and rotation (start at 0/0/0)
  if isempty(Tr_0_inv)
    Tr_0_inv = inv([R t;0 0 0 1]);
  end
      
  % add pose
  pose{pose_index} = Tr_0_inv*[R t;0 0 0 1];
  
  % rot to cam frame
  R = pose{pose_index}(1:3,1:3);
  t = pose{pose_index}(1:3,4);
  
  R = DS.Payloads.Mono_Camera.R_B_to_C*R*DS.Payloads.Mono_Camera.R_B_to_C';
  t = DS.Payloads.Mono_Camera.R_B_to_C*t + DS.Payloads.Mono_Camera.t_C_to_B_C;
  pose{pose_index}(1:3,1:3) = R;
  pose{pose_index}(1:3,4) = t;
end
DS.GroundTruth.data = pose;

%% Create trajectory from GT
% % Create trajectory for the first step
%   GT_global_1 = gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{1}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{1}(1:3,4)));
%   GT_global_0 = gtsam.Pose3(gtsam.Rot3(Rot),gtsam.Point3(origin));
% % Store trajectory for the first step 
%   DS.GroundTruth.trajectory{1} = GT_global_0.between(GT_global_1);
% trajectory for the rest of the course
for pose_index = 2 : max_index
    GT_global_k1 = gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{pose_index}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{pose_index}(1:3,4)));
    GT_global_k =  gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{pose_index-1}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{pose_index-1}(1:3,4)));
    % Store the trajectory
    DS.GroundTruth.trajectory{pose_index} = GT_global_k.between(GT_global_k1);
end
%% upload measurements 
defname = '0000000000';
max_index = min(max_index , size(dir([DS.Payloads.Mono_Camera.Path '/*.mat']),1) - 1);
for i= 1:max_index
    %uploading Mono_Camera measurements
    frame_name = [defname(1:end- length(num2str(i))) num2str(i)];
    load([DS.Payloads.Mono_Camera.Path '/' frame_name '.mat'])
    desc = d;
    measurements = f;
    DS.Payloads.Mono_Camera.data{i} = measurements;
    DS.Payloads.Mono_Camera.desc{i} = desc;
    
    % uploading raw pic
    if flag.loadRawImages
        DS.Payloads.Mono_Camera.pic{i} = imread([DS.Payloads.Mono_Camera.Path '/' frame_name '.png']);
    end
    %uploading Lidar measurements
    DS.Payloads.Lidar.data{i} = [];
    %uploading Stereo_Camera measurements
    DS.Payloads.Stereo_Camera.data{i} = [];
    %uploading IMU measurements
    DS.Payloads.IMU.data{i} = [];
end
%% upload DA
DS.DA.Correspondence_mat = data.Correspondence_mat;
%% adjust Correspondence matrix
% zeros or nan in R matrix
for clear_landmark_index = [15 17 40 95 99 102 127 190 314 489 492 518 617 715 734 779 945]
    DS.DA.Correspondence_mat(clear_landmark_index,:) = 0;
end
% too large estimation values
for clear_landmark_index = [93 96 120 125 214 552]
    DS.DA.Correspondence_mat(clear_landmark_index,:) = 0;
end
% underdetermind error
for clear_landmark_index = [30720]
    DS.DA.Correspondence_mat(clear_landmark_index,:) = 0;
end
% % the same estimation as a diff landmark
% for clear_landmark_index = [7 32 35 56 57 86 156 247 337 352 389 408 462 475 476 491 506 508 ]
%     DS.DA.Correspondence_mat(clear_landmark_index,:) = 0;
% end
%% Finish up
% save([DS.dataset_path 'DS'], 'DS');
disp('## Finished calculating, storing & loading Dataset ##')
end

