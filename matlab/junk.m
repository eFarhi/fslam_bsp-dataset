clear classes
clear all
clc
%% Prepare the Database 
[DS] = ibsp_ds09_LoadDataset();
DA = DataAssociation;
DA.Payloads.Mono_Camera.noise_model = gtsam.noiseModel.Diagonal.Sigmas([1;1]);


for pose_index = 1 : 3
    CurrentReading = ibsp_ds10_GetMeasurements(DS,pose_index,DS.OptionalPayloads.Mono_Camera);
%     [DA,values,factors] = ibsp_ds08_DataAssociation(DA,CurrentReading,pose_index); 
        current_pose = pose_index;
    %% Storing the information
        DA.Payloads.Mono_Camera.data{current_pose} = CurrentReading.Measurements;
        %% locating descriptor matching
        if isempty(CurrentReading.Correspondence)
%             index_vec = ibsp_ds11_DescriptorMatching();
%             DA = bla_bla;
        else
            DA.Payloads.Mono_Camera.Correspondence_Mat(:,current_pose,1) = CurrentReading.Correspondence;
            index = DA.Payloads.Mono_Camera.Correspondence_Mat(:,current_pose,1) > 0;
            DA.Payloads.Mono_Camera.Correspondence_Mat(index,current_pose,2) = 1;
            DA.Payloads.Mono_Camera.Descriptors(:,index) = CurrentReading.Descriptor(:,DA.Payloads.Mono_Camera.Correspondence_Mat(index,current_pose,1));
        end
        %% Check if a Landmark can be triangulated
        values = gtsam.Values;
        factors = gtsam.NonlinearFactorGraph;
        feature_occurrences = sum(DA.Payloads.Mono_Camera.Correspondence_Mat(:,1:current_pose,2),2) > 1;
        if sum(feature_occurrences)
            %% Triangulate Landmark
           [values,factors] = ibsp_ds12_TriangulateLandmark(DA,feature_occurrences,current_pose,values,factors,CurrentReading.Payload);
        else
           % not enough features to triangulate a landmark 
        end
    

end