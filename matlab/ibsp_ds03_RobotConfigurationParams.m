function RobotSettings = ibsp_ds03_RobotConfigurationParams(DS)
%%

%% Rotation matrices

R_G_to_B = DCM_construct( [0,0,0] ); % Note - G is NED
R_B_to_C = DS.Payloads.Mono_Camera.R_B_to_C;
% R_B_to_C = DCM_construct([0 0 90]*pi/180);    % downward pointing camera
% R_B_to_C = DCM_construct([90 0 90]*pi/180);    % forward pointing camera
RobotSettings.R_G_to_C =  R_B_to_C* R_G_to_B;
RobotSettings.R_B_to_C = R_B_to_C;
RobotSettings.t_C_to_B_C = DS.Payloads.Mono_Camera.t_C_to_B_C; % camera translation in ref to the Body
RobotSettings.t_C_to_G_C = R_G_to_B*RobotSettings.t_C_to_B_C;

R_B_to_IMU = DCM_construct([0 0 0]*pi/180);    % IMU coordinate system
RobotSettings.R_G_to_IMU = R_B_to_IMU * R_G_to_B;
RobotSettings.t_IMU_to_B_IMU = [0.81 -0.32 0]'; % IMU translation in ref to the Body

%% Robot Origin

RobotSettings.origin.loc = DS.GroundTruth.data{1}(1:3,4); 
RobotSettings.origin.rot = DS.GroundTruth.data{1}(1:3,1:3);
%% Movement
V            = 50;
V_B          = [V; 0; 0];
RobotSettings.V_C = R_B_to_C * V_B; 
RobotSettings.V   = V;
RobotSettings.yawRate = 20 * pi/180; %rad/sec
RobotSettings.dt = 1.0; % basic navigation\slam simulation step

%% Camera Calibretion
RobotSettings.Camera.aperture = 90; % [deg]
RobotSettings.Camera.MaximumObservRange = 40; % [m]. Maximum range to being able to observe a landmark
RobotSettings.Camera.image_hieght = 376; %[pix]
RobotSettings.Camera.image_width = 1241; %[pix]
RobotSettings.Camera.Sigma_Im_Noise = 0.5;

%% Noise Models

RobotSettings.ang_sigma = [0.5, 0.5, 0.5]*pi/180;
RobotSettings.translation_sigma = 5e-2*[1, 1, 1]; %[10, 10, 10]; % TODO: cancel small noise
RobotSettings.image_noise_sigma = [RobotSettings.Camera.Sigma_Im_Noise, RobotSettings.Camera.Sigma_Im_Noise];
RobotSettings.odometry_sigma    = [RobotSettings.ang_sigma, RobotSettings.translation_sigma];
RobotSettings.range_sigma       = 1.0;
RobotSettings.initial_ang_sigma = [0.1, 0.1, 0.1]*pi/180;
RobotSettings.initial_pos_sigma = [0.1, 0.1, 0.1]*pi/180;
RobotSettings.initial_pose_sigma = [RobotSettings.initial_ang_sigma, RobotSettings.initial_pos_sigma];
RobotSettings.initial_landmark_sigma = gtsam.noiseModel.Diagonal.Sigmas([60 60 60]');

RobotSettings.odometry_noise_model = gtsam.noiseModel.Diagonal.Sigmas(RobotSettings.odometry_sigma');
RobotSettings.image_noise_model    = gtsam.noiseModel.Diagonal.Sigmas(RobotSettings.image_noise_sigma');
RobotSettings.range_model          = gtsam.noiseModel.Diagonal.Sigmas(RobotSettings.range_sigma);

end
