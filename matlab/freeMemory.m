function freeMem = freeMemory(type)
    [r, w] = unix(['free | grep ', type]);
    stats = str2double(regexp(w, '[0-9]*', 'match'));
    memsize = stats(1)/1e6;

    if numel(stats) > 3
        freeMem = (stats(3)+stats(end))/1e6;
    else
        freeMem = stats(3)/1e6;
    end

%totalFree = freeMemory('Mem') + freeMemory('Swap')