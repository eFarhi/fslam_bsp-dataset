function [ OutputData ] = ibsp_ds14_UpdateOutput( OutputData,method_index,belief, result,estimate,newValues,newFactors,pose_index,FLAGS,keys,manuever_delta_vec)
%IBSP_DS14_UPDATEOUTPUT updates the output file
%% saving the belief
% saving from index 1, hence pose_index 
OutputData.method{method_index}.result = result;
OutputData.method{method_index}.estimate = estimate;
% OutputData.method{method_index}.belief = gtsam.ISAM2(belief);
OutputData.method{method_index}.newFactors = newFactors.size;
OutputData.method{method_index}.newValues = newValues.size;
OutputData.method{method_index}.Estimates.Poses = gtsam.utilities.extractPose3(estimate);
OutputData.method{method_index}.pose_index = pose_index;

%% calculating & storing the cov
% marginals = gtsam.Marginals(belief.getFactorsUnsafe, estimate);
% for i = 1:length(keys.pose_keys_vec)
%     pose_key_i = keys.pose_keys_vec(i);
%     cov = marginals.marginalCovariance(pose_key_i);
%     cov = TransformMarginalCovToGlobalFrame(cov, pose_key_i, pose_key_i, estimate);    
%     cov_robot_position{i} = cov(4:6,4:6);
% end
% OutputData.method{method_index}.cov_robot_position = cov_robot_position;

%% storing timing


%% storing
OutputData.keys = keys;

% OutputData.cov_incr_tr_vec        = trace(cov);
OutputData.manuever_delta_vec = manuever_delta_vec;
% OutputData.EstErrAtGoal           = [];
% OutputData.MaxPositionCovTrace    = FLAGS.MaxPositionCovTrace;
OutputData.FLAGS                  = FLAGS;
% OutputData.mean_pos_goal_all      = mean_pos_goal_all;




end


function cov_global = TransformMarginalCovToGlobalFrame(cov_local, key1, key2, values)

% Transform position covariance of poses to a gloval frame
M1 = eye(size(cov_local,1));
M2 = eye(size(cov_local,2));

if char(gtsam.mrsymbolChr(key1)) == 'x'
    R1 = values.at(key1).rotation.matrix;
    M1 = [eye(3), zeros(3,3);
        zeros(3,3), R1];
end
if char(gtsam.mrsymbolChr(key2)) == 'x'
    R2 = values.at(key2).rotation.matrix;
    M2 = [eye(3), zeros(3,3);
        zeros(3,3), R2];
end
cov_global = M1 * cov_local * M2' ;
end
