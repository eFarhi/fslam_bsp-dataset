function Output = ibsp_ds15_CompareFG(varargin)
%% Compare fg1 to fg2, no order importance, not limited to number of involved per factor
%% Input variables
counter = length(varargin);
fg1 = varargin{1};
fg2 = varargin{2};
rep_calc = varargin{3};
if counter == 4
    return_fg = 1;
    fg1_values = varargin{3};
else
    fg1_values = [];
    return_fg = 0;
end
compareFG_time = 0;
for rep = 1: rep_calc
    %% init
    AdjMat_fg1.mat = [];
    AdjMat_fg1.mat = sparse(AdjMat_fg1.mat);
    AdjMat_fg1.fac_mat = {};
    AdjMat_fg1.priors = {};
    AdjMat_fg2.mat = [];
    AdjMat_fg2.mat = sparse(AdjMat_fg2.mat);
    AdjMat_fg2.fac_mat = {};
    AdjMat_fg2.priors = {};
    key_vec = [];
    if return_fg
        new_factors = gtsam.NonlinearFactorGraph;
        new_values = gtsam.Values;
    end
    %% Build adjacency matrix for fg1
    for i = 0 : fg1.size-1
        fac = fg1.at(i);
        key = fac.keys;
        key_size = key.size;
        connection_indx = [];
        for j = 0 : key_size-1
            key_at_j = key.at(j);
            exists = find(key_vec == key_at_j);
            if isempty(exists)
                key_vec = [key_vec ; key_at_j];
                connection_indx = [connection_indx ; length(key_vec)];
            else
                connection_indx = [connection_indx ; exists];
            end
        end
        if length(AdjMat_fg1.mat)< max(connection_indx)
            AdjMat_fg1.mat(max(connection_indx),max(connection_indx)) = 0;
        end
        AdjMat_fg1.mat(connection_indx,connection_indx) = AdjMat_fg1.mat(connection_indx,connection_indx) + 1;
        
        if key_size == 1
            if length(AdjMat_fg1.priors) < connection_indx
                AdjMat_fg1.priors{connection_indx} = [];
            end
            AdjMat_fg1.priors{connection_indx} = [AdjMat_fg1.priors{connection_indx},i];
        end
        
        if length(AdjMat_fg1.fac_mat)< max(connection_indx)
            AdjMat_fg1.fac_mat{max(connection_indx),max(connection_indx)} = [];
        end
        for indx1 = 1 : length(connection_indx)
            for indx2 = 1 : length(connection_indx)
                AdjMat_fg1.fac_mat{connection_indx(indx1),connection_indx(indx2)} = [AdjMat_fg1.fac_mat{connection_indx(indx1),connection_indx(indx2)},i];
            end
        end
    end
    %% Build adjacency matrix for fg2
    for i = 0 : fg2.size-1
        fac = fg2.at(i);
        key = fac.keys;
        key_size = key.size;
        connection_indx = [];
        for j = 0 : key_size-1
            key_at_j = key.at(j);
            exists = find(key_vec == key_at_j);
            if isempty(exists)
                key_vec = [key_vec ; key_at_j];
                connection_indx = [connection_indx ; length(key_vec)];
            else
                connection_indx = [connection_indx ; exists];
            end
        end
        if length(AdjMat_fg2.mat)< max(connection_indx)
            AdjMat_fg2.mat(max(connection_indx),max(connection_indx)) = 0;
        end
        AdjMat_fg2.mat(connection_indx,connection_indx) = AdjMat_fg2.mat(connection_indx,connection_indx) + 1;
        
        if key_size == 1
            if length(AdjMat_fg2.priors) < connection_indx
                AdjMat_fg2.priors{connection_indx} = [];
            end
            AdjMat_fg2.priors{connection_indx} = [AdjMat_fg2.priors{connection_indx},i];
        end
        
        if length(AdjMat_fg2.fac_mat)< max(connection_indx)
            AdjMat_fg2.fac_mat{max(connection_indx),max(connection_indx)} = [];
        end
        for indx1 = 1 : length(connection_indx)
            for indx2 = 1 : length(connection_indx)
                AdjMat_fg2.fac_mat{connection_indx(indx1),connection_indx(indx2)} = [AdjMat_fg2.fac_mat{connection_indx(indx1),connection_indx(indx2)},i];
            end
        end
    end
    %% Match Sizes
    length1 = length(AdjMat_fg1.mat(1,:));
    length2 = length(AdjMat_fg2.mat(1,:));
    if length1 > length2
        AdjMat_fg2.mat(length1,length1) = 0;
    elseif length1 < length2
        AdjMat_fg1.mat(length2,length2) = 0;
    end
    tic
    %% Mark Diff
    AdjMatComp_1minus2 = AdjMat_fg1.mat-AdjMat_fg2.mat;
    AdjMatComp_1minus2_output = AdjMatComp_1minus2;
    [row,column] = find(AdjMatComp_1minus2 > 0);
    in1_notin2 = [row,column];
    [row,column] = find(AdjMatComp_1minus2 < 0);
    in2_notin1 = [row,column];
    new_factor_list = [];
    bad_factor_list = [];
    if isempty(in1_notin2) && isempty(in2_notin1)
        match = true;
        time = toc;
    else
        match = false;
        %% Find mismatched Factors
        diag_list = [];
        for indx = 1 : length(in1_notin2(:,1))
            i = in1_notin2(indx,1);
            j = in1_notin2(indx,2);
            if j>i
                % Off Diagonal New Factors
                counter = AdjMatComp_1minus2(i,j);
                if counter
                    new_factor_list((end+1) : (end+abs(counter))) = AdjMat_fg1.fac_mat{i,j}(1:abs(counter));
                    AdjMatComp_1minus2([i,j],[i,j]) = AdjMatComp_1minus2([i,j],[i,j]) - counter;
                end
            elseif j==i
                if isempty(find(diag_list==i,1))
                    diag_list(end+1) = i;
                end
            end
        end
        for indx = 1 : length(in2_notin1(:,1))
            i = in2_notin1(indx,1);
            j = in2_notin1(indx,2);
            if j>i
                % Off Diagonal Bad Factors
                counter = AdjMatComp_1minus2(i,j);
                if counter
                    bad_factor_list((end+1) : (end+abs(counter))) = AdjMat_fg2.fac_mat{i,j}(1:abs(counter));
                    AdjMatComp_1minus2([i,j],[i,j]) = AdjMatComp_1minus2([i,j],[i,j]) - counter;
                end
            elseif j==i
                if isempty(find(diag_list==i,1))
                    diag_list(end+1) = i;
                end
            end
        end
        
        % Diagonal Factors - priors
        for i = 1: length(diag_list)
            indx = diag_list(i);
            counter = AdjMatComp_1minus2(indx,indx);
            if counter > 0
                % new prior
                new_factor_list(end+1) = AdjMat_fg1.priors{indx}(1:abs(counter));
            elseif counter < 0
                % bad prior
                bad_factor_list(end+1) = AdjMat_fg2.priors{indx}(1:abs(counter));
            end
        end
        new_fac_with_new_values = 0;
        time = toc;
        if return_fg
            % create a fg out of the new good factors
            for i = 1: length(new_factor_list)
                new_factors.add(fg1.at(new_factor_list(i)));
                if ~isempty(fg1_values)
                    factor = fg1.at(new_factor_list(i));
                    keys = factor.keys;
                    flag_new_fac_with_new_values = 0;
                    for k = 0: keys.size -1
                        %                if ~new_values.exists(keys.at(k))
                        try
                            new_values.insert(keys.at(k),fg1_values.at(keys.at(k)));
                            flag_new_fac_with_new_values = 1;
                        catch
                        end
                    end
                    if flag_new_fac_with_new_values
                        new_fac_with_new_values = new_fac_with_new_values +1;
                    end
                end
            end
        end
    end
    compareFG_time = compareFG_time +time;
end
compareFG_time = compareFG_time/rep_calc;
%% create output file
Output.compareFG_time = compareFG_time;
Output.AdjMat_fg1 = AdjMat_fg1;
Output.AdjMat_fg2 = AdjMat_fg2;
Output.AdjMatComp_1minus2 = AdjMatComp_1minus2_output;
Output.scaffolding = AdjMatComp_1minus2;
Output.match = match;
Output.in1_notin2 = in1_notin2;
Output.in2_notin1 = in2_notin1;
Output.key_vec = key_vec;
Output.new_factors_indxIn1 = new_factor_list;
if return_fg
    Output.new_factors_from1 = new_factors;
    Output.new_values_from1 = new_values;
    Output.new_fac_with_new_values = new_fac_with_new_values;
else
    Output.new_factors_from1 = [];
    Output.new_values_from1 = [];
    Output.new_fac_with_new_values = [];
end
Output.bad_factors_indxIn2 = bad_factor_list;
Output.fg2_size = fg2.size;










