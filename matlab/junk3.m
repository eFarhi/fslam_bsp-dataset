%% fixing odom
% clear classes
% clear all
% clc
% [ DS ] = ibsp_ds09_LoadDataset;

%% create the belief
isam_optimization = gtsam.ISAM2DoglegParams;
isam_optimization.setWildfireThreshold(0.001);
isam_params = gtsam.ISAM2Params;

warning('changing optimization config');
isam_optimization = gtsam.ISAM2GaussNewtonParams;
isam_params.setRelinearizeThreshold(0);
isam_optimization.setWildfireThreshold(0);

isam_params.setOptimizationParams(isam_optimization);
isam_params.setFactorization('qr');
isam_params.setRelinearizeSkip(1);
isam_params.setEnableRelinearization(true);
isam_params.setEnablePartialRelinearizationCheck(false);

belief = gtsam.ISAM2(isam_params);
%%
fig_handle.main = figure;
fig_handle.estimate = [];
fig_handle.gt = [];
hold on
axis equal
set(gcf,'color','white');
% xlabel(fig_config.label.x);
% ylabel(fig_config.label.y);
% zlabel(fig_config.label.z);
%% init manuv
newFactors = gtsam.NonlinearFactorGraph;
newValues  = gtsam.Values;
pose_curr_key = gtsam.symbol('x', 1);
initial_pose = gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{1}(1:3,1:3)), gtsam.Point3(DS.GroundTruth.data{1}(1:3,4)));
factor = gtsam.PriorFactorPose3(pose_curr_key, initial_pose, gtsam.noiseModel.Diagonal.Sigmas(1e-5*ones(6,1)));
newFactors.push_back(factor);
newValues.insert(pose_curr_key, initial_pose);

keys.pose_keys_vec = pose_curr_key;
keys.landmark_keys_vec = [];

result   = belief.update(newFactors, newValues);
estimate = belief.calculateEstimate();


for pose_index = 2:2500
    %% init
    pose_prev_key = pose_curr_key;
    pose_curr_key = gtsam.symbol('x',pose_index);
    newFactors = gtsam.NonlinearFactorGraph;
    newValues  = gtsam.Values;
    %% clean odom
    odometry = DS.GroundTruth.trajectory{pose_index};
    %% noise to odom
    odometry_sigma = [0.0087    0.0087    0.0087    0.0500    0.0500    0.0500]';
%     odometry_sigma = zeros(6,1);
    noise = mvnrnd(zeros(size(odometry_sigma')), odometry_sigma'.^2)';
    odometry_measured = odometry.retract(noise);
%     odometry_measured = odometry;
    %% create factor
    odometry_noise_model = gtsam.noiseModel.Diagonal.Sigmas(odometry_sigma);
    odom_fac = gtsam.BetweenFactorPose3(pose_prev_key, pose_curr_key, odometry_measured, odometry_noise_model);
    newFactors.push_back(odom_fac);
    pose_predict = estimate.at(pose_prev_key).compose(odometry_measured);
    newValues.insert(pose_curr_key, pose_predict);
    
    %% get belief estimate
    estimate_before_update = belief.calculateEstimate();
    estimate_before_update.insert(newValues);
    belief.update(newFactors, newValues);
    for s=1:5
        belief.update;
    end
    estimate = belief.calculateBestEstimate();
    
    %% plot
    % plot GT
    if pose_index == 1
        GT = [DS.GroundTruth.origin(1:3,4)' ;DS.GroundTruth.data{pose_index}(1:3,4)'];
    else
        GT = [DS.GroundTruth.data{pose_index-1}(1:3,4)' ;DS.GroundTruth.data{pose_index}(1:3,4)'];
    end
    figure(fig_handle.main);
    hold on
    fig_handle.gt = plot3(GT(:,1),GT(:,3), -GT(:,2) , '-', 'Color', 'blue');
    set(fig_handle.gt,'MarkerSize',3,'LineWidth',2);
    
    % plot estimation
    pose = gtsam.utilities.extractPose3(estimate);
    pose = pose(:,10:12);
    delete(fig_handle.estimate)
    figure(fig_handle.main);
    hold on
    fig_handle.estimate = plot3(pose(:,1), pose(:,3),  -pose(:,2), '--', 'Color', 'red');
    set(fig_handle.estimate,'MarkerSize',3,'LineWidth',2);
    drawnow;
end

%%
clear all
close all
clc

%% 
full_mat = rand(10001,10001);
b = rand(10001,1);
leng = length(full_mat(:,1));
bulk = floor(leng / 23);
spair = (leng / 23 - bulk)*23;
% bulk = 5000;
counter = 1;
while counter*bulk <= length(b)
    mat{counter} = full_mat(1+(counter-1)*bulk:counter*bulk,:);
    counter = counter+1;
end
counter = counter-1;
if counter*bulk < length(b)
    mat{counter+1} = full_mat(1+counter*bulk:end,:);
end
mat1 = mat{1};
mat2 = mat{2};
mat3 = mat{3};
mat4 = mat{4};
mat5 = mat{5};
mat6 = mat{6};
mat7 = mat{7};
mat8 = mat{8};
mat9 = mat{9};
mat10 = mat{10};
mat11 = mat{11};
% mat{1} = mat1;
% mat{2} = mat2;
% mat{3} = mat3;
% mat{4} = mat4;
% mat{5} = mat5;

time1 = 0;
time2 = 0;
time21 = 0;
time22 = 0;
rep = 100;
check2 = [];
for i = 1 : rep
    tic
        check1 = full_mat*b;
    time1 = time1+ toc;
    a = zeros(size(mat{11}));
    tic
        check22 = [mat{1}*b;mat{2}*b;mat{3}*b;mat{4}*b;mat{5}*b;mat{6}*b;mat{7}*b;mat{8}*b;mat{9}*b;mat{10}*b;mat{11}*b;a*b;a*b;a*b;a*b;a*b;a*b;a*b;a*b];
    time21 = time21 + toc;

        tic
        check22 = [mat{1}*b;mat{2}*b;mat{3}*b;mat{4}*b;mat{5}*b;mat{6}*b;mat{7}*b;mat{8}*b;mat{9}*b;mat{10}*b;mat{11}*b];
    time22 = time22 + toc;

    for j = 1:length(mat)
        tic
        check2 = [check2;mat{j}*b];
        time2 = time2+ toc;
    end
%         check2 = [mat{1}*b;mat{2}*b;mat{3}*b;mat{4}*b;mat{5}*b;mat];
    
end
time1 = time1/rep;
time21 = time21/rep;
time22 = time22/rep;
time2 = time2/rep;


tic
[mat{1}*b;mat{2}*b;mat{3}*b;mat{4}*b;mat{5}*b;mat{6}*b;mat{7}*b;mat{8}*b;mat{9}*b;mat{10}*b];
toc