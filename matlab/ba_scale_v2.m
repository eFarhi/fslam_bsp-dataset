% Vladimir Ovechkin December 2016

% Old version "ba_scale.m" is difficult to maintain compatible for both: 
% with old and with new tasks. New version "v2" will obtain new features:
% - scale for detected objects
% - flexible noise models
% - option for tukey noise model

function [FinalError,RealTimeError,INError,FinalPosition,RTPosition,GTPosition, CheckedWhiteList,bList_of_scales,optimization_time] = ba_scale_v2(data,subset_of_poses,whiteList_scale_idxs)

if nargin <3
   whiteList_scale_idxs = find(sum(data.Correspondence_mat,2));
end

if isfield (data.Settings,'SelfClean')
    if (data.Settings.SelfClean > 0)
        data.Settings.OBJ_SCALE_ON = 0;
    end
end

if (data.Settings.Plotting.show3DPoints), h_track = figure; end

if isfield(data.Settings.Thresholds,'max_range_scale')
    max_range_scale = data.Settings.Thresholds.max_range_scale;
else
    max_range_scale = 99999999; % large number
end

% update calibration matrix
K_Cal3_S2 = gtsam.Cal3_S2(data.Calibration.fx * data.Settings.scale, ...
    data.Calibration.fy * data.Settings.scale, ...
    data.Calibration.skew, ...
    data.Calibration.c_x * data.Settings.scale, ...
    data.Calibration.c_y * data.Settings.scale);

% gtsam preperations
values = gtsam.Values;
valuesGT = gtsam.Values;    % values object to store GT
valuesRT = gtsam.Values;    % values object to store Real Time error
%valuesIN = gtsam.Values;    % values object to store initialization values only 
valuesINEWRTEST = gtsam.Values; % values object to store initialization values only 
RTCov = [];     % list of covarance matricies for the last pose real time
graph  = gtsam.NonlinearFactorGraph;

image_noise_model = gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.image_noise_sigma*[1 1]');
if data.Settings.noise.robust_features > 0
    model_Tukey  = gtsam.noiseModel.mEstimator.Tukey(data.Settings.noise.robust_features);
    image_noise_model = gtsam.noiseModel.Robust.Create(model_Tukey, image_noise_model);
end

if isfield(data.Settings.noise,'obj_uv_noise_sigma')
    object_uv_noise_model = gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.obj_uv_noise_sigma*[1 1]');
    if data.Settings.noise.robust_objects > 0
        model_Tukey  = gtsam.noiseModel.mEstimator.Tukey(data.Settings.noise.robust_objects);
        object_uv_noise_model = gtsam.noiseModel.Robust.Create(model_Tukey, object_uv_noise_model);
    end
end

if isfield(data.Settings, 'MSER_FEATURES_ON')
    mser_uv_noise_model = gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.mser_uv_noise_sigma*[1 1]');
    if data.Settings.noise.robust_mser > 0
        model_Tukey  = gtsam.noiseModel.mEstimator.Tukey(data.Settings.noise.robust_mser);
        mser_uv_noise_model = gtsam.noiseModel.Robust.Create(model_Tukey, mser_uv_noise_model);
    end
    
    mser_scale_noise_model = gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.mser_scale_noise_sigma);
    if (data.Settings.noise.robust_mser > 0)
        mser_model_Tukey = gtsam.noiseModel.mEstimator.Tukey(data.Settings.noise.robust_mser);
        mser_scale_noise_model = gtsam.noiseModel.Robust.Create(mser_model_Tukey, mser_scale_noise_model);
    end
end

if isfield(data.Settings.noise,'robust_objects')
    if data.Settings.noise.robust_objects > 0
        object_model_Tukey = gtsam.noiseModel.mEstimator.Tukey(data.Settings.noise.robust_objects);
    end
end


%temp = full(data.Correspondence_mat);   % debug variable
array_of_f_scale = [];
array_of_idxs_for_f_scale = [];
past_poses = [];
scaleSigmaVector = [];
first_frame = data.Settings.offset+1;
prev_i = first_frame - 1;

%list_of_frames = first_frame : data.Settings.step : first_frame+data.Settings.m-1;
i = first_frame;
while i <= first_frame+data.Settings.m-1
%for i = list_of_frames%(1:(length(list_of_frames)-1)) - first_frame + 1   %loop by frames
%     if i>400
%         data.Settings.OBJ_SCALE_ON = 1;
%     end

%     if  i==385 data.Settings.SCALE_ON = 0; end;
%     if  i==420 data.Settings.SCALE_ON = 1; end;

    if isfield(data.Settings,'SelfClean')
        oldGraph = graph.clone;
        oldValues = gtsam.Values;
        oldValues.insert(values);
    end

    % init new pose
    disp(['processing pose ',sprintf('%d',i)]);
    pose_key = gtsam.symbol('x', i);
    pose_i_GT = subset_of_poses{length (past_poses) + first_frame};
    
    if ~valuesGT.exists(pose_key)
        valuesGT.insert(pose_key,pose_i_GT);
    end
    
    if i == first_frame
        % add prior factor
        values.insert(pose_key,pose_i_GT);
        %valuesIN.insert(pose_key,pose_i_GT);
        graph.add(gtsam.PriorFactorPose3(pose_key, pose_i_GT, gtsam.noiseModel.Diagonal.Sigmas([1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4]')));
    else
        % read previous pose from values optimized 1 step before to create corrupted GT
        pose_key_prev = gtsam.symbol('x',prev_i);
        pose_i_prev_GT = valuesGT.at(pose_key_prev);
        pose_rel_GT = pose_i_prev_GT.between(pose_i_GT);
        pose_i_prev = values.at(pose_key_prev);
        
        % add range factor
        if prev_i == first_frame
            range = norm(pose_i_GT.translation.vector - pose_i_prev_GT.translation.vector);   %calculate NOT corrupted range
            f_range = gtsam.RangeFactorPose3(pose_key_prev, pose_key, range, gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.range_sigma));
            graph.push_back(f_range);
        end
        
        % check if VO exists. If 'YES' - 'data.Settings.CORRUPT_INITIAL_POSE' means nothing.
        if isfield (data.Settings,'VO')
            poseCur = gtsam.Pose3(data.Settings.VO(:,:,gtsam.symbolIndex(pose_key)));
            posePrev = gtsam.Pose3(data.Settings.VO(:,:,gtsam.symbolIndex(pose_key_prev)));
            pose_rel = posePrev.between(poseCur);
        else
            % corrupt pose_rel_GT with noise
            if (data.Settings.CORRUPT_INITIAL_POSE & (i > 3*data.Settings.step))
                if isfield(data.Settings.noise,'initial')
                    [rel_rot_n , rel_t_n] = makeNoisePoseFromGT( pose_rel_GT.rotation , pose_rel_GT.translation, 'seed', ...
                        data.Settings.noise.initial.rotation(:,i),data.Settings.noise.initial.translation(:,i));
                else
                    [rel_rot_n , rel_t_n] = makeNoisePoseFromGT( pose_rel_GT.rotation , pose_rel_GT.translation, 'rnd', ...
                        data.Settings.noise.rotation_sigma,data.Settings.noise.position_sigma);
                end
                pose_rel_GT_with_noise = gtsam.Pose3(rel_rot_n,rel_t_n);
            end
            
            if exist ('pose_rel_GT_with_noise','var')
                pose_rel = pose_rel_GT_with_noise;
            else
                pose_rel = pose_rel_GT;
            end
        end
        %valuesIN.insert(pose_key,valuesIN.at(pose_key_prev).compose(pose_rel));
        pose_i = pose_i_prev.compose(pose_rel);

        %add new pose to values
        values.insert(pose_key,pose_i);
        if ~valuesINEWRTEST.exists(pose_key)
            valuesINEWRTEST.insert( pose_key,pose_i );
        end
    end
    
    if i>first_frame
        list_of_new_landmarks_in_i = [];
        FactorsAddedCounter = 0;
        
        % loop for all landmarks observed in current frame
        list_of_all_l = 1:size(data.Correspondence_mat,1);
        for j = list_of_all_l(data.Correspondence_mat(:,i)>0)
        
        % loop for previously observed landmarks
        %for j = 1:size(data.Correspondence_mat,1) % old
        
        %for j = (1: find(data.Correspondence_mat(:,i)>0))
            l_key = gtsam.symbol('l',j);
            l_id = j;
            
            if (values.exists(l_key))&& (data.Correspondence_mat(j,i) > 0)
                % add factor for current pose_i
                pixel_real_i = data.Views_cell{i}.pixels(1:2,data.Correspondence_mat(l_id,i))';
                gtsamPixel_i = gtsam.Point2(pixel_real_i');
                f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel_i, image_noise_model, pose_key, l_key, K_Cal3_S2);
                graph.push_back( f_proj );
                
                if data.Settings.SCALE_ON
                    % check if the feature scale in lthe blacklist (is inconsistent)
                    if ( find ( whiteList_scale_idxs ==  l_id ))
                        %check: do not add scale factor if landmark is further than threshold
                        if (max_range_scale > norm(pose_i.transform_to(values.at(l_key)).vector))
                            if strcmp (data.Settings.SCALE_TYPE , 'explicit')
                                ObjSize_key = gtsam.symbol('s',j);
                                if (values.exists(ObjSize_key))
                                    f_scale = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key, l_key, ObjSize_key,...
                                        data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i)), ...
                                        K_Cal3_S2, gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.scale_noise_sigma));
                                    graph.push_back(f_scale);
                                    array_of_f_scale = [array_of_f_scale , f_scale];
                                    array_of_idxs_for_f_scale = [array_of_idxs_for_f_scale , j];
                                end
                            end
                            if strcmp (data.Settings.SCALE_TYPE , 'implicit')
                                if (sum(data.Correspondence_mat(j,:)>0) > data.Settings.Thresholds.min_landmark_length) %check for global landmark length
                                    if (values.exists(l_key))
                                        prev_pose_idx = max(find(data.Correspondence_mat(l_id,1:i-1)>0));
                                        prev_pose_key = gtsam.symbol('x',prev_pose_idx);
                                        
                                        scale_i = data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i));
                                        scale_i_prev = data.Views_cell{prev_pose_idx}.pixels(3,data.Correspondence_mat(l_id,prev_pose_idx));
                                        
                                        scaleSigma = data.Settings.noise.scale_noise_sigma*(1/scale_i^2 + scale_i_prev^2/scale_i^4);
                                        
                                        f_scale = lba.TwoViewFeatureScaleFactorPose3Point3(pose_key, prev_pose_key, l_key, ...
                                            data.Views_cell{prev_pose_idx}.pixels(3,data.Correspondence_mat(l_id,prev_pose_idx)), ...
                                            data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i)), ...
                                            gtsam.noiseModel.Diagonal.Sigmas(scaleSigma));
                                        scaleSigmaVector = [scaleSigmaVector,scaleSigma];
                                        if (scaleSigma > data.Settings.Thresholds.min_scale_noise_sigma)
                                            graph.push_back(f_scale);
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
                FactorsAddedCounter=FactorsAddedCounter+1;
            end
        end
        
        if (FactorsAddedCounter==0)
            fac = gtsam.BetweenFactorPose3(pose_key,pose_key_prev,pose_rel,gtsam.noiseModel.Diagonal.Sigmas(1.0e-4 * [ 1 1 1 1 1 1]'));
            
            %factor is never added
        end
        values_before_opt = gtsam.Values;
        values_before_opt.insert(values);
        optimizer = gtsam.LevenbergMarquardtOptimizer(graph, values);
        values    = optimizer.optimize;
            
        % loop by landmarks for triangulating new ones
        %for j = 1:size(data.Correspondence_mat,1)      old
        % loop for all landmarks observed in current frame
        list_of_all_l = 1:size(data.Correspondence_mat,1);
        for j = list_of_all_l(data.Correspondence_mat(:,i)>0)
            l_key = gtsam.symbol('l',j);
            l_id = j;
            
            % try to triangulate new landmarks
            if ((sum(data.Correspondence_mat(j,[past_poses , i]) >0 ) >= 2 ) && (data.Correspondence_mat(j,i)>0) && (~(values.exists(l_key))))
                % find x2
                Corr = data.Correspondence_mat(j,past_poses );  %choose only correspondencies until i for landmark j
                poses_with_j = find(Corr > 0);
                
                pixel_real_i = data.Views_cell{i}.pixels(1:2,data.Correspondence_mat(l_id,i))';
                
                %for k = poses_with_j((length(poses_with_j)-1):-1:first_frame)   % try all possible pairs to triangulate new landmark
                %for k = flipud (past_poses(poses_with_j))
                for k = fliplr (past_poses(poses_with_j))
                %for k = flipud(poses_with_j((poses_with_j >= 1) & (poses_with_j~=i)))
                    pixel_real_i_prev = data.Views_cell{k}.pixels(1:2,data.Correspondence_mat(l_id,k))';
                    
                    % take out pose with previous matched landmark
                    pose_key_prev = gtsam.symbol('x',k);
                    pose_i_prev = values.at(pose_key_prev);
                    
                    % too small motion between cameras -> skip
                    if norm(pose_i_prev.between(pose_i).translation.vector) < data.Settings.Thresholds.poses_shift
                        continue;
                    end
                    
                    % too small landmark shift -> skip
                    if norm(pixel_real_i_prev - pixel_real_i) < data.Settings.Thresholds.min_pixel_shift
                        continue;
                    end
                    
                    if norm(pixel_real_i_prev - pixel_real_i) > data.Settings.Thresholds.max_pixel_shift
                        continue;
                    end;
                    
                    calib_pix_i_prev = K_Cal3_S2.calibrate(gtsam.Point2(pixel_real_i_prev(1), pixel_real_i_prev(2)));
                    calib_pix_i = K_Cal3_S2.calibrate(gtsam.Point2(pixel_real_i(1), pixel_real_i(2)));
                    
                    pose_key_prev = gtsam.symbol('x',k);
                    pose_i_prev = values.at(pose_key_prev);
                    
                    landmark_init_CAM1 = lba.triangulateDLT(pose_i_prev, pose_i, calib_pix_i_prev, calib_pix_i);
                    
                    % check if triangulated landmark is good
%                     landmark_init_pose_i_vector = pose_i.translation.between(landmark_init_CAM1).vector;
%                     fake_pose = gtsam.Pose3(gtsam.Rot3.RzRyRx(0,0,0),landmark_init_CAM1);
%                     landmark_in_frame_i = pose_i.compose(fake_pose).translation.vector;
                     checkPoint1 = pose_i_prev.transform_to(landmark_init_CAM1).vector;
                     checkPoint2 = pose_i.transform_to(landmark_init_CAM1).vector;
                    
%                     % triangulation gives result in cam1 frame
%                     landmark_in_pose_i_prev_frame = lba.triangulateDLT(pose_i_prev, pose_i, calib_pix_i_prev, calib_pix_i);
%                     % check if triangulated landmark is good
%                     landmark_in_global = landmark
%                     fake_pose_i_prev = gtsam.Pose3(gtsam.Rot3(RxRyRx(0,0,0),gtsam.Point3(landmark_init_pose_i_vector)
                    
                    
%                     if ((norm(pose_i.translation.between(landmark_init_CAM1).vector) > data.Settings.Thresholds.range_near) ... 
%                             && (norm(pose_i.translation.between(landmark_init_CAM1).vector) < data.Settings.Thresholds.range_far) ...
%                             && (checkPoint(3)>0))

                    % loop closure check. Set LC flag to add landmark anycase
                    loop_closure_flag=0;
                    line = data.Correspondence_mat(j,:);
                    num = find(line);
                    if (num(length(num))-num(1)>500)
                        loop_closure_flag = 1;
                    end
                    if data.Settings.SelfClean>0, loop_closure_flag = 0; end % to switch off flag in Cleaning mode
                    %end of LC check
                    
                    if    ((norm(pose_i.translation.between(landmark_init_CAM1).vector) > data.Settings.Thresholds.range_near) ...
                        && (norm(pose_i.translation.between(landmark_init_CAM1).vector) < data.Settings.Thresholds.range_far) ...
                        && (norm(pose_i_prev.translation.between(landmark_init_CAM1).vector) > data.Settings.Thresholds.range_near) ... %% bug was found 2017.03.14
                        && (norm(pose_i_prev.translation.between(landmark_init_CAM1).vector) < data.Settings.Thresholds.range_far) ...
                        && (checkPoint1(3)>0) ...
                        && (checkPoint2(3)>0))%||(loop_closure_flag) % uncomment the previous comment to use LC-flag
                            
                            %%&& (landmark_init_pose_i_vector(3) < pose_i.translation.z)) % do not work for KITTI !!!!
                            
                        % add landmark to values
                        values.insert(l_key, landmark_init_CAM1);
                        list_of_new_landmarks_in_i = [list_of_new_landmarks_in_i,l_id];
                    else
                        continue;
                    end
                    
                    % add factors to graph
                    gtsamPixel_i_prev = gtsam.Point2(pixel_real_i_prev');
                    f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel_i_prev, image_noise_model, pose_key_prev, l_key, K_Cal3_S2);
                    graph.push_back( f_proj );
                    gtsamPixel_i = gtsam.Point2(pixel_real_i');
                    f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel_i, image_noise_model, pose_key, l_key, K_Cal3_S2);
                    graph.push_back( f_proj );
                    if data.Settings.SCALE_ON
                         % check if the feature scale in the whitelist (is consistent)
                         if (find ( whiteList_scale_idxs ==  l_id ))
                            if (norm(pose_i_prev.translation.between(landmark_init_CAM1).vector) > max_range_scale), break; end;
                            if sum(data.Correspondence_mat(j,:)>0) < data.Settings.Thresholds.min_landmark_length, break; end; %global landmark length
                            if strcmp (data.Settings.SCALE_TYPE , 'explicit')
                                % fx only value is taken for simplisity as fx is approximately equal to fy - should be fixed in future
                                initalSize_i = norm(pose_i.translation.between(landmark_init_CAM1).vector) * ...
                                    data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i)) / K_Cal3_S2.fx;
                                initalSize_i_prev = norm(pose_i_prev.translation.between(landmark_init_CAM1).vector) * ...
                                    data.Views_cell{k}.pixels(3,data.Correspondence_mat(l_id,k)) / K_Cal3_S2.fx;
                                averageSize = (initalSize_i + initalSize_i_prev)/2;
                                
                                ObjSize_key = gtsam.symbol('s',l_id);
                                values.insert(ObjSize_key, gtsam.LieScalar(averageSize));
                                valuesRT.insert(ObjSize_key, gtsam.LieScalar(averageSize));
                                f_scale_i = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key, l_key, ObjSize_key,...
                                    data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i)), ...
                                    K_Cal3_S2, gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.scale_noise_sigma));
                                
                                array_of_f_scale = [array_of_f_scale, f_scale_i];
                                array_of_idxs_for_f_scale = [array_of_idxs_for_f_scale , j];
                                
                                f_scale_i_prev = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key_prev, l_key, ObjSize_key,...
                                    data.Views_cell{k}.pixels(3,data.Correspondence_mat(l_id,k)), ...
                                    K_Cal3_S2, gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.scale_noise_sigma));
                                
                                array_of_f_scale = [array_of_f_scale, f_scale_i_prev];
                                array_of_idxs_for_f_scale = [array_of_idxs_for_f_scale , j];
                                
                                graph.push_back(f_scale_i);
                                graph.push_back(f_scale_i_prev);
                            end
                            if strcmp (data.Settings.SCALE_TYPE , 'implicit')
                                
                                scale_i = data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i));
                                scale_i_prev = data.Views_cell{k}.pixels(3,data.Correspondence_mat(l_id,k));
                                
                                scaleSigma = data.Settings.noise.scale_noise_sigma*(1/scale_i^2 + scale_i_prev^2/scale_i^4);
                                
                                f_scale = lba.TwoViewFeatureScaleFactorPose3Point3 (pose_key_prev, pose_key, l_key, ...
                                    data.Views_cell{k}.pixels(3,data.Correspondence_mat(l_id,k)), ...
                                    data.Views_cell{i}.pixels(3,data.Correspondence_mat(l_id,i)), ...
                                    gtsam.noiseModel.Diagonal.Sigmas(scaleSigma));
                                scaleSigmaVector = [scaleSigmaVector,scaleSigma];
                                if (scaleSigma > data.Settings.Thresholds.min_scale_noise_sigma)
                                    graph.push_back(f_scale);
                                end
                            end
                        end
                    end
                    break;
                end
            end
        end

        % additional plot
        if data.Settings.Plotting.show3DPoints
            if exist( 'h_track', 'var')
            else
                h_track = figure;
            end
            
            hold off;
            
            plot3(0 ,0 ,0, 'bx');
            hold on;
            
            pose_track = plot_optimized_track(values, past_poses ,h_track,'b');
            plot_optimized_track(valuesGT, past_poses ,h_track,'k');
            plot_optimized_track(valuesINEWRTEST, past_poses ,h_track,'g');
%             pointcloud_all = plot_landmarks(values, 1:size(data.Correspondence_mat,1) ,h_track,'k',true);
            
            plot3(0 ,0 ,0, 'bx'); % shows origin
            xlabel('x'); ylabel('y'); zlabel('z');
            axis('equal');
            %axis ([-100, 100, -20, 20, -100, 100]);
        end
        
        % graph optimization with new landmarks
        optimizer = gtsam.LevenbergMarquardtOptimizer(graph, values);
        optim_start = tic;
        values    = optimizer.optimize;
        optimization_time(i) = toc(optim_start);
    end
    
%     %% check block
%     [coord,index] = LandmarkPositions(values);
%     [~,imin]=min(coord(3,:))
%     data.Correspondence_mat(index(imin),:) = 0; 
%     save([data.Settings.base_dir,CashedDataFile],'data');
%     %%
    
    prev_i = i;
    if (sum (past_poses == i) == 0)
        past_poses = [past_poses , i];
    end
    
    if ~valuesRT.exists(pose_key)
        valuesRT.insert( pose_key, values.at(pose_key) );
    end
    %Covariance
    %{
    try 
        marginals = gtsam.Marginals(graph,values);
        RTCov{i} = [RTCov,{marginals.marginalCovariance(pose_key)}];
    catch
        warning(['Problem estimating covariance at pose ',sprintf('%d',i)]);
    end
    %}
    
    % Plotting 3D map
    if data.Settings.Plotting.show3DPoints
        if exist( 'h_track', 'var')
        else
            h_track = figure;
        end
        
        % hold off;
        
        %plot3(0 ,0 ,0, 'bx');
        %      h_track = figure;
        hold on;
        
        pose_track = plot_optimized_track(values, past_poses ,h_track,'b');
        plot_optimized_track(valuesGT, past_poses ,h_track,'k');
        % plot_optimized_track(valuesINEWRTEST, past_poses ,h_track,'g');
        % pointcloud_all = plot_landmarks(values, 1:size(data.Correspondence_mat,1) ,h_track,'k',false);
        
        % plot3(0 ,0 ,0, 'bx'); % shows origin
        xlabel('x'); ylabel('y'); zlabel('z');
        axis('equal');
        %axis ([-350, 150, -150, 150, -150, 20]);
    end
    
    % % object-scale block
    % {
    
    if isfield(data.Settings, 'OBJ_SCALE_ON')
        if (data.Settings.OBJ_SCALE_ON ==1)
            all_object_indexes = (1:1:size(data.object_Correspondence_matrix , 1));
            for obj_idx = all_object_indexes(data.object_Correspondence_matrix(:,i) > 0)
                obj_key = gtsam.symbol('o',obj_idx);
                ObjWidth_key = gtsam.symbol('w',obj_idx);
                % condition: if object was observed more then minimal number of times
                if sum(data.object_Correspondence_matrix(obj_idx,1:i)>0) > data.Settings.Thresholds.min_object_length
                    % add scale and projection factors only !
                    % add object projection factor
                    obj_prop = data.Views_cell{i}.objects(:,data.object_Correspondence_matrix(obj_idx,i));
                        gtsamPixel = gtsam.Point2(obj_prop(1),obj_prop(2));
                        f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel, object_uv_noise_model, pose_key, obj_key, K_Cal3_S2);
                        graph.push_back( f_proj );
                        
                        % add object scale factor
                        curr_obj_scale_noise_model = gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.obj_scale_noise_sigma/obj_prop(5));
                        if (data.Settings.noise.robust_objects > 0)
                            curr_obj_scale_noise_model = gtsam.noiseModel.Robust.Create(object_model_Tukey, curr_obj_scale_noise_model);
                        end
                        
                        f_scale_width = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key, obj_key, ObjWidth_key,...
                            data.Views_cell{i}.objects(3,data.object_Correspondence_matrix(obj_idx,i)), ...
                            K_Cal3_S2, curr_obj_scale_noise_model);
                        graph.push_back( f_scale_width );
                else
                    % condition: if object was observed the exact minimal number of times
                    if sum(data.object_Correspondence_matrix(obj_idx,1:i)>0) == data.Settings.Thresholds.min_object_length
                        % triangulate and add virtual landmark
                        all_frame_indexes = (1:1:size(data.object_Correspondence_matrix , 2));
                        prev_pose_idx = min(all_frame_indexes(data.object_Correspondence_matrix(obj_idx,1:i)>0));
                        curr_pose_idx = max(all_frame_indexes(data.object_Correspondence_matrix(obj_idx,1:i)>0));
                        
                        prev_obj_UV = data.Views_cell{prev_pose_idx}.objects(:,data.object_Correspondence_matrix(obj_idx,prev_pose_idx));
                        curr_obj_UV = data.Views_cell{curr_pose_idx}.objects(:,data.object_Correspondence_matrix(obj_idx,curr_pose_idx));
                        
                        calib_pix_i_prev = K_Cal3_S2.calibrate(gtsam.Point2(prev_obj_UV(1), prev_obj_UV(2)));
                        calib_pix_i_curr = K_Cal3_S2.calibrate(gtsam.Point2(curr_obj_UV(1), curr_obj_UV(2)));
                        
                        pose_key_prev = gtsam.symbol('x',prev_pose_idx);
                        pose_key = gtsam.symbol('x',curr_pose_idx);
                        
                        pose_obj_prev = values.at(pose_key_prev);
                        pose_obj_curr = values.at(pose_key);
                        

                        virt_landmark_init_CAM1 = lba.triangulateDLT(pose_obj_prev, pose_obj_curr, calib_pix_i_prev, calib_pix_i_curr);
                        
                        initalSize_i_prev = norm(pose_obj_prev.translation.between(virt_landmark_init_CAM1).vector) * prev_obj_UV(3) / K_Cal3_S2.fx;
                        initalSize_i_curr = norm(pose_obj_curr.translation.between(virt_landmark_init_CAM1).vector) * curr_obj_UV(3) / K_Cal3_S2.fx;
                        averageWidth = (initalSize_i_prev + initalSize_i_curr)/2;
                        
                        values.insert(obj_key, virt_landmark_init_CAM1);
                        values.insert(ObjWidth_key, gtsam.LieScalar(averageWidth));
                        
                        % add factors for all previous observations
                        for iter_fr = all_frame_indexes(data.object_Correspondence_matrix(obj_idx,1:i)>0)
                            obj_prop = data.Views_cell{iter_fr}.objects(:,data.object_Correspondence_matrix(obj_idx,iter_fr));
                            gtsamPixel = gtsam.Point2(obj_prop(1),obj_prop(2));
                            pose_key_obj = gtsam.symbol('x',iter_fr);
                                % add object projection factor
                                f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel, object_uv_noise_model, pose_key_obj, obj_key, K_Cal3_S2);
                                graph.push_back( f_proj );
                                
                                % add object scale factor
                                curr_obj_scale_noise_model = gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.obj_scale_noise_sigma/obj_prop(5));
                                if (data.Settings.noise.robust_objects > 0)
                                    curr_obj_scale_noise_model = gtsam.noiseModel.Robust.Create(object_model_Tukey, curr_obj_scale_noise_model);
                                end
                                f_scale_width = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key_obj, obj_key, ObjWidth_key,...
                                    data.Views_cell{iter_fr}.objects(3,data.object_Correspondence_matrix(obj_idx,iter_fr)), ...
                                    K_Cal3_S2, curr_obj_scale_noise_model);
                                
                                graph.push_back( f_scale_width );
                        end
                    end
                end
            end
        end
    end
    %}
    
    if isfield(data.Settings, 'MSER_FEATURES_ON')
        if (data.Settings.MSER_FEATURES_ON==1)
            all_mser_indexes = (1:1:size(data.Correspondence_mat_mser , 1));
            for mser_idx = all_mser_indexes(data.Correspondence_mat_mser(:,i) > 0)
                mser_key = gtsam.symbol('m',mser_idx);
                MserWidth_key = gtsam.symbol('a',mser_idx); % a - axis
                % condition: if mser was observed more then minimal number of times
                if sum(data.Correspondence_mat_mser(mser_idx,1:i)>0) > data.Settings.Thresholds.min_mser_length
                    % add scale and projection factors only !
                    % add mser projection factor
                    mser_prop = data.Views_cell_mser{i}.pixels(:,data.Correspondence_mat_mser(mser_idx,i)); % change
                        gtsamPixel = gtsam.Point2(mser_prop(1),mser_prop(2));
                        f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel, mser_uv_noise_model, pose_key, mser_key, K_Cal3_S2);
                        graph.push_back( f_proj );
                        
                        % add mser scale factor
                                                
                        f_scale_width = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key, mser_key, MserWidth_key,...
                            data.Views_cell_mser{i}.pixels(3,data.Correspondence_mat_mser(mser_idx,i)), ...
                            K_Cal3_S2, mser_scale_noise_model);
                        graph.push_back( f_scale_width );
                else
                    % condition: if object was observed the exact minimal number of times
                    if sum(data.Correspondence_mat_mser(mser_idx,1:i)>0) == data.Settings.Thresholds.min_mser_length
                        % triangulate and add virtual landmark
                        all_frame_indexes = (1:1:size(data.Correspondence_mat_mser , 2));
                        prev_pose_idx = min(all_frame_indexes(data.Correspondence_mat_mser(mser_idx,1:i)>0));
                        curr_pose_idx = max(all_frame_indexes(data.Correspondence_mat_mser(mser_idx,1:i)>0));
                        
                        prev_mser_UV = data.Views_cell_mser{prev_pose_idx}.pixels(:,data.Correspondence_mat_mser(mser_idx,prev_pose_idx));
                        curr_mser_UV = data.Views_cell_mser{curr_pose_idx}.pixels(:,data.Correspondence_mat_mser(mser_idx,curr_pose_idx));
                        
                        calib_pix_i_prev = K_Cal3_S2.calibrate(gtsam.Point2(prev_mser_UV(1), prev_mser_UV(2)));
                        calib_pix_i_curr = K_Cal3_S2.calibrate(gtsam.Point2(curr_mser_UV(1), curr_mser_UV(2)));
                        
                        pose_key_prev = gtsam.symbol('x',prev_pose_idx);
                        pose_key = gtsam.symbol('x',curr_pose_idx);
                        
                        pose_obj_prev = values.at(pose_key_prev);
                        pose_obj_curr = values.at(pose_key);
                        

                        virt_landmark_init_CAM1 = lba.triangulateDLT(pose_obj_prev, pose_obj_curr, calib_pix_i_prev, calib_pix_i_curr);
                        
                        initalSize_i_prev = norm(pose_obj_prev.translation.between(virt_landmark_init_CAM1).vector) * prev_mser_UV(3) / K_Cal3_S2.fx;
                        initalSize_i_curr = norm(pose_obj_curr.translation.between(virt_landmark_init_CAM1).vector) * curr_mser_UV(3) / K_Cal3_S2.fx;
                        averageWidth = (initalSize_i_prev + initalSize_i_curr)/2;
                        
                        values.insert(mser_key, virt_landmark_init_CAM1);
                        values.insert(MserWidth_key, gtsam.LieScalar(averageWidth));
                        
                        % add factors for all previous observations
                        for iter_fr = all_frame_indexes(data.Correspondence_mat_mser(mser_idx,1:i)>0)
                            mser_prop = data.Views_cell_mser{iter_fr}.pixels(:,data.Correspondence_mat_mser(mser_idx,iter_fr));
                            gtsamPixel = gtsam.Point2(mser_prop(1),mser_prop(2));
                            pose_key_obj = gtsam.symbol('x',iter_fr);
                                % add mser projection factor
                                f_proj = gtsam.GenericProjectionFactorCal3_S2(gtsamPixel, mser_uv_noise_model, pose_key_obj, mser_key, K_Cal3_S2);
                                graph.push_back( f_proj );
                                
                                % add mser scale factor
                                f_scale_width = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key_obj, mser_key, MserWidth_key,...
                                    data.Views_cell_mser{iter_fr}.pixels(3,data.Correspondence_mat_mser(mser_idx,iter_fr)), ...
                                    K_Cal3_S2, mser_scale_noise_model);
                                
                                graph.push_back( f_scale_width );
                        end
                    end
                end
            end
        end
    end
    
    if i >= data.Settings.STOP_i, break; end
    if (isfield(data.Settings,'SelfClean'))
        if (data.Settings.SelfClean > 0)
            if (data.Settings.SelfClean <= i)
                listOfBadLandmarks = UWerror_detector(graph,values);
                disp(['Number of rejected landmarks ',sprintf('%d',length(listOfBadLandmarks))]);
                if (length (listOfBadLandmarks) > 5)
                        x = 1;
                end
                if (sum(sum (full(data.Correspondence_mat(listOfBadLandmarks,:))))==0) && (~isempty(listOfBadLandmarks))
                    if (data.Settings.SelfClean > 50) 
                        data.Settings.SelfClean = data.Settings.SelfClean - 49;
                    end
                    return
                else
                    data.Correspondence_mat(listOfBadLandmarks,:) = 0;
                end
                if length(listOfBadLandmarks) > 0
                    save([data.Settings.imagesFolder,data.Settings.selfName],'data');
                    graph = oldGraph.clone;
                    values.clear
                    values.insert(oldValues);
                    i = i-data.Settings.step;
                    prev_i = i - data.Settings.step;
                else
                    data.Settings.SelfClean = data.Settings.SelfClean + 1; % changing step number for a new cleaning stage
                end
            end
        end
    end
    i = i+data.Settings.step;
end % for all frames cycle


% Error estimation
FinalError = EstError(values,valuesGT);
RealTimeError = EstError(valuesRT,valuesGT);
INError = EstError(valuesINEWRTEST,valuesGT);
% plot_est_error( FinalError, RTError, INError, '-b', '*g', '-g','Final error','Real-Time error','Initial error');

for idx = 1:length(array_of_f_scale)
    UnWEr(idx) = array_of_f_scale(idx).unwhitenedError(values);
end

for i = first_frame:data.Settings.STOP_i
    pose_key = gtsam.symbol('x',i);
    FinalPosition(:,i) = values.at(pose_key).translation.vector;
    GTPosition(:,i) = valuesGT.at(pose_key).translation.vector;
    RTPosition(:,i) = valuesRT.at(pose_key).translation.vector;
end

wList_of_Scales = [];

% {
if data.Settings.SCALE_reject_bad_feature_scale
    %data.Settings.SCALE_reject_bad_feature_scale = 0;
    % Plotting 3D map
    %{
        h_track = figure;
        hold off;
        
        plot3(0 ,0 ,0, 'bx');
        hold on;
        
        pose_track = plot_optimized_track(values, past_poses ,h_track,'b');
        plot_optimized_track(valuesGT, past_poses ,h_track,'k');
        plot_optimized_track(valuesINEWRTEST, past_poses ,h_track,'g');
        
        plot3(0 ,0 ,0, 'bx'); % shows origin
        xlabel('x'); ylabel('y'); zlabel('z');
        axis('equal');
        %axis ([-350, 150, -150, 150, -150, 20]);
    %}
        [wList_of_Scales,bList_of_scales] = UWerror_detector_FScale(graph,values);
        CheckedWhiteList = [];
        % {
        % old check if whiteList landmark is good
        for iter = 1:length(wList_of_Scales)
            if (min( find(data.Correspondence_mat(wList_of_Scales(iter),:) )) > data.Settings.offset)
                if (max( find(data.Correspondence_mat(wList_of_Scales(iter),:) )) < data.Settings.STOP_i)
                    CheckedWhiteList = [CheckedWhiteList,wList_of_Scales(iter)];
                end
            end
        end
        %}
       disp(sprintf('%d consistent scale landmarks are approved(old check)',length(CheckedWhiteList)));
 %     data.Correspondence_mat(checklist,:) = 0;
 %     [FinalError,RealTimeError,INError,FinalPosition,RTPosition,GTPosition] = ba_scale_v2(data,subset_of_poses);
else 
    CheckedWhiteList = [];
    bList_of_scales=[];
end
%}

% 
%{ 
data.Settings.feature_scale_cleaning_mode = 0;
if data.Settings.feature_scale_cleaning_mode
    UWerrors = sparse(size(data.Correspondence_mat,1),size(data.Correspondence_mat,2))
    
    tmpGraph = gtsam.NonlinearFactorGraph;
    
    tmpValues = gtsam.Values;
    
    for l_id = 1 : size(data.Correspondence_mat,1)
        l_key = gtsam.symbol('l', l_id);
        if (values.exists(l_key))
             tmpGraph = graph.clone;
             tmpValues.clear;
             tmpValues.insert(values);
             
             frames = find(data.Correspondence_mat(l_id, first_frame:data.Settings.STOP_i));
             if length(frames) < data.Settings.Thresholds.min_landmark_length
                 continue;
             end
             % initialize size using 2 poses
             pose_i = values.at(gtsam.symbol('x',frames(1)));
             pose_i_prev = values.at(gtsam.symbol('x',frames(2)));
             landmark = values.at(gtsam.symbol('l',l_id));
             
             initalSize_i = norm(pose_i.translation.between(landmark).vector) * ...
                 data.Views_cell{frames(1)}.pixels(3,data.Correspondence_mat(l_id,frames(1))) / K_Cal3_S2.fx;
             initalSize_i_prev = norm(pose_i_prev.translation.between(landmark).vector) * ...
                 data.Views_cell{frames(2)}.pixels(3,data.Correspondence_mat(l_id,frames(2))) / K_Cal3_S2.fx;
             averageSize = (initalSize_i + initalSize_i_prev)/2;
             
             ObjSize_key = gtsam.symbol('s',l_id);
             tmpValues.insert(ObjSize_key, gtsam.LieScalar(averageSize));
             
             f_list = {};
             for frame = frames
                 % add all factors
                 pose_key = gtsam.symbol('x',frame);
                 f_scale = lba.FeatureScaleFactorPose3Point3LieScalar(pose_key, l_key, ObjSize_key,...
                     data.Views_cell{frame}.pixels(3,data.Correspondence_mat(l_id,frame)), ...
                     K_Cal3_S2, gtsam.noiseModel.Diagonal.Sigmas(data.Settings.noise.scale_noise_sigma));
                 tmpGraph.push_back(f_scale);
                 f_list{length(f_list)+1} = f_scale;
                 % continue here
             end
        
             tmpOptimizer = gtsam.LevenbergMarquardtOptimizer(tmpGraph, tmpValues);
             tmpValues    = tmpOptimizer.optimize;
             
             % estimate UW error, save it to list
             for i = 1 : length(f_list)
                 factor = f_list{i};
                 factorKeys = factor.keys;
                 frameIdx = gtsam.symbolIndex(factorKeys.front);
                 featureIdx = gtsam.symbolIndex(factorKeys.back);
                 UWerrors(featureIdx,frameIdx) = abs(factor.unwhitenedError(tmpValues));
             end
             
        end
        
    end
    
    mean(UWerrors,2)
    % estimate average UW error
    % detect bad feature scales
end
%}

%Covariance
%{
try
    for i = past_poses
    pose_key = gtsam.symbol('x', i);
    marginals = gtsam.Marginals(graph,values);
    FinalCov{i} = marginals.marginalCovariance(pose_key);min(all_frame_indexes(data.object_Correspondence_matrix(obj_index,1:i)>0))
    end
catch
    warning('Problem estimating final covariance.');
end
%}
%lba.TwoViewFeatureScaleFactorPose3Point3(pose_key, size_t poseKey2, size_t pointKey, double feature_scale1, double feature_scale2, Base noiseModel)