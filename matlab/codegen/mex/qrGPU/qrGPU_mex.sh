MATLAB="/Applications/MATLAB_R2017b.app"
Arch=maci64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/Users/Issac/Library/Application Support/MathWorks/MATLAB/R2017b"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for qrGPU" > qrGPU_mex.mki
echo "CC=$CC" >> qrGPU_mex.mki
echo "CFLAGS=$CFLAGS" >> qrGPU_mex.mki
echo "CLIBS=$CLIBS" >> qrGPU_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> qrGPU_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> qrGPU_mex.mki
echo "CXX=$CXX" >> qrGPU_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> qrGPU_mex.mki
echo "CXXLIBS=$CXXLIBS" >> qrGPU_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> qrGPU_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> qrGPU_mex.mki
echo "LDFLAGS=$LDFLAGS" >> qrGPU_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> qrGPU_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> qrGPU_mex.mki
echo "Arch=$Arch" >> qrGPU_mex.mki
echo "LD=$LDXX" >> qrGPU_mex.mki
echo OMPFLAGS= >> qrGPU_mex.mki
echo OMPLINKFLAGS= >> qrGPU_mex.mki
echo "EMC_COMPILER=clang" >> qrGPU_mex.mki
echo "EMC_CONFIG=optim" >> qrGPU_mex.mki
"/Applications/MATLAB_R2017b.app/bin/maci64/gmake" -B -f qrGPU_mex.mk
