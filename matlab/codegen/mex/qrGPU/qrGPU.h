/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * qrGPU.h
 *
 * Code generation for function 'qrGPU'
 *
 */

#ifndef QRGPU_H
#define QRGPU_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "qrGPU_types.h"

/* Function Declarations */
extern void qrGPU(const emlrtStack *sp, const emxArray_real_T *A,
                  emxArray_real_T *Q, emxArray_real_T *R, emxArray_real_T *E);

#endif

/* End of code generation (qrGPU.h) */
