/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_qrGPU_api.h
 *
 * Code generation for function '_coder_qrGPU_api'
 *
 */

#ifndef _CODER_QRGPU_API_H
#define _CODER_QRGPU_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "qrGPU_types.h"

/* Function Declarations */
extern void qrGPU_api(const mxArray * const prhs[1], const mxArray *plhs[3]);

#endif

/* End of code generation (_coder_qrGPU_api.h) */
