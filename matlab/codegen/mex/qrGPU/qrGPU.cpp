/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * qrGPU.cpp
 *
 * Code generation for function 'qrGPU'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qrGPU.h"
#include "qrGPU_emxutil.h"
#include "error.h"
#include "eml_int_forloop_overflow_check.h"
#include "xgeqp3.h"
#include "qrGPU_data.h"
#include "lapacke.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 3,     /* lineNo */
  "qrGPU",                             /* fcnName */
  "/Users/Issac/ANPL/code/fSLAM_BSP-dataset/matlab/qrGPU.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 17,  /* lineNo */
  "qr",                                /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/qr.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 23,  /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 94,  /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 95,  /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 99,  /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 100, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 105, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 107, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 113, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 114, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 117, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 121, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 124, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 128, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 129, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 132, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 136, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 137, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 141, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 142, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 143, /* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo qb_emlrtRSI = { 38, /* lineNo */
  "xorgqr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xorgqr.m"/* pathName */
};

static emlrtRSInfo rb_emlrtRSI = { 46, /* lineNo */
  "xorgqr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xorgqr.m"/* pathName */
};

static emlrtRSInfo sb_emlrtRSI = { 51, /* lineNo */
  "xorgqr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xorgqr.m"/* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 59, /* lineNo */
  "xorgqr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xorgqr.m"/* pathName */
};

static emlrtRSInfo ub_emlrtRSI = { 207,/* lineNo */
  "eml_qr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pathName */
};

static emlrtRSInfo vb_emlrtRSI = { 14, /* lineNo */
  "xorgqr",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xorgqr.m"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 23,  /* lineNo */
  9,                                   /* colNo */
  "eml_qr",                            /* fName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/matfun/private/eml_qr.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 1, /* lineNo */
  20,                                  /* colNo */
  "qrGPU",                             /* fName */
  "/Users/Issac/ANPL/code/fSLAM_BSP-dataset/matlab/qrGPU.m"/* pName */
};

/* Function Definitions */
void qrGPU(const emlrtStack *sp, const emxArray_real_T *A, emxArray_real_T *Q,
           emxArray_real_T *R, emxArray_real_T *E)
{
  int32_T m;
  int32_T n;
  int32_T info;
  int32_T unnamed_idx_1;
  int32_T i0;
  emxArray_real_T *tau;
  emxArray_int32_T *jpvt;
  emxArray_real_T *b_A;
  boolean_T overflow;
  int32_T loop_ub;
  emxArray_int32_T *jpvt1;
  ptrdiff_t info_t;
  boolean_T p;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  emlrtStack i_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  i_st.prev = &h_st;
  i_st.tls = h_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /* QRGPU preforms qr factorization */
  st.site = &emlrtRSI;
  b_st.site = &b_emlrtRSI;
  c_st.site = &c_emlrtRSI;
  m = A->size[0];
  n = A->size[1];
  info = A->size[0];
  unnamed_idx_1 = A->size[0];
  i0 = Q->size[0] * Q->size[1];
  Q->size[0] = info;
  Q->size[1] = unnamed_idx_1;
  emxEnsureCapacity_real_T(&c_st, Q, i0, &emlrtRTEI);
  i0 = R->size[0] * R->size[1];
  R->size[0] = A->size[0];
  R->size[1] = A->size[1];
  emxEnsureCapacity_real_T(&c_st, R, i0, &emlrtRTEI);
  emxInit_real_T1(&c_st, &tau, 1, &b_emlrtRTEI, true);
  emxInit_int32_T(&c_st, &jpvt, 2, &b_emlrtRTEI, true);
  if (A->size[0] > A->size[1]) {
    d_st.site = &d_emlrtRSI;
    overflow = ((!(1 > A->size[1])) && (A->size[1] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = 0; info + 1 <= n; info++) {
      d_st.site = &e_emlrtRSI;
      if ((!(1 > m)) && (m > 2147483646)) {
        e_st.site = &w_emlrtRSI;
        f_st.site = &w_emlrtRSI;
        check_forloop_overflow_error(&f_st);
      }

      for (unnamed_idx_1 = 0; unnamed_idx_1 + 1 <= m; unnamed_idx_1++) {
        Q->data[unnamed_idx_1 + Q->size[0] * info] = A->data[unnamed_idx_1 +
          A->size[0] * info];
      }
    }

    d_st.site = &f_emlrtRSI;
    overflow = ((!(A->size[1] + 1 > A->size[0])) && (A->size[0] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = A->size[1]; info + 1 <= m; info++) {
      d_st.site = &g_emlrtRSI;
      if ((!(1 > m)) && (m > 2147483646)) {
        e_st.site = &w_emlrtRSI;
        f_st.site = &w_emlrtRSI;
        check_forloop_overflow_error(&f_st);
      }

      for (unnamed_idx_1 = 1; unnamed_idx_1 <= m; unnamed_idx_1++) {
        Q->data[(unnamed_idx_1 + Q->size[0] * info) - 1] = 0.0;
      }
    }

    emxInit_int32_T(&c_st, &jpvt1, 2, &b_emlrtRTEI, true);
    d_st.site = &h_emlrtRSI;
    xgeqp3(&d_st, Q, tau, jpvt1);
    i0 = jpvt->size[0] * jpvt->size[1];
    jpvt->size[0] = 1;
    jpvt->size[1] = A->size[1];
    emxEnsureCapacity_int32_T(&c_st, jpvt, i0, &emlrtRTEI);
    d_st.site = &i_emlrtRSI;
    overflow = ((!(1 > A->size[1])) && (A->size[1] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = 0; info + 1 <= n; info++) {
      jpvt->data[info] = jpvt1->data[info];
    }

    emxFree_int32_T(&jpvt1);
    d_st.site = &j_emlrtRSI;
    overflow = ((!(1 > A->size[1])) && (A->size[1] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = 0; info + 1 <= n; info++) {
      d_st.site = &k_emlrtRSI;
      for (unnamed_idx_1 = 0; unnamed_idx_1 + 1 <= info + 1; unnamed_idx_1++) {
        R->data[unnamed_idx_1 + R->size[0] * info] = Q->data[unnamed_idx_1 +
          Q->size[0] * info];
      }

      d_st.site = &l_emlrtRSI;
      for (unnamed_idx_1 = info + 1; unnamed_idx_1 + 1 <= m; unnamed_idx_1++) {
        R->data[unnamed_idx_1 + R->size[0] * info] = 0.0;
      }
    }

    d_st.site = &m_emlrtRSI;
    e_st.site = &ub_emlrtRSI;
    f_st.site = &vb_emlrtRSI;
    if (!((Q->size[0] == 0) || (Q->size[1] == 0))) {
      g_st.site = &qb_emlrtRSI;
      h_st.site = &hb_emlrtRSI;
      g_st.site = &rb_emlrtRSI;
      h_st.site = &hb_emlrtRSI;
      g_st.site = &sb_emlrtRSI;
      h_st.site = &mb_emlrtRSI;
      info_t = LAPACKE_dorgqr(102, (ptrdiff_t)A->size[0], (ptrdiff_t)A->size[0],
        (ptrdiff_t)A->size[1], &Q->data[0], (ptrdiff_t)A->size[0], &tau->data[0]);
      info = (int32_T)info_t;
      g_st.site = &tb_emlrtRSI;
      h_st.site = &nb_emlrtRSI;
      if (info != 0) {
        overflow = true;
        p = false;
        if (info == -7) {
          p = true;
        } else {
          if (info == -5) {
            p = true;
          }
        }

        if (!p) {
          if (info == -1010) {
            h_st.site = &ob_emlrtRSI;
            i_st.site = &ob_emlrtRSI;
            error(&i_st);
          } else {
            h_st.site = &pb_emlrtRSI;
            i_st.site = &pb_emlrtRSI;
            c_error(&i_st, info);
          }
        }
      } else {
        overflow = false;
      }

      if (overflow) {
        i0 = Q->size[0] * Q->size[1];
        emxEnsureCapacity_real_T(&f_st, Q, i0, &b_emlrtRTEI);
        loop_ub = Q->size[1];
        for (i0 = 0; i0 < loop_ub; i0++) {
          info = Q->size[0];
          for (unnamed_idx_1 = 0; unnamed_idx_1 < info; unnamed_idx_1++) {
            Q->data[unnamed_idx_1 + Q->size[0] * i0] = rtNaN;
          }
        }
      }
    }
  } else {
    emxInit_real_T(&c_st, &b_A, 2, &b_emlrtRTEI, true);
    i0 = b_A->size[0] * b_A->size[1];
    b_A->size[0] = A->size[0];
    b_A->size[1] = A->size[1];
    emxEnsureCapacity_real_T(&c_st, b_A, i0, &b_emlrtRTEI);
    loop_ub = A->size[0] * A->size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
      b_A->data[i0] = A->data[i0];
    }

    d_st.site = &n_emlrtRSI;
    xgeqp3(&d_st, b_A, tau, jpvt);
    d_st.site = &o_emlrtRSI;
    overflow = ((!(1 > A->size[0])) && (A->size[0] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = 0; info + 1 <= m; info++) {
      d_st.site = &p_emlrtRSI;
      if (info + 1 > 2147483646) {
        e_st.site = &w_emlrtRSI;
        f_st.site = &w_emlrtRSI;
        check_forloop_overflow_error(&f_st);
      }

      for (unnamed_idx_1 = 0; unnamed_idx_1 + 1 <= info + 1; unnamed_idx_1++) {
        R->data[unnamed_idx_1 + R->size[0] * info] = b_A->data[unnamed_idx_1 +
          b_A->size[0] * info];
      }

      d_st.site = &q_emlrtRSI;
      if ((!(info + 2 > m)) && (m > 2147483646)) {
        e_st.site = &w_emlrtRSI;
        f_st.site = &w_emlrtRSI;
        check_forloop_overflow_error(&f_st);
      }

      for (unnamed_idx_1 = info + 1; unnamed_idx_1 + 1 <= m; unnamed_idx_1++) {
        R->data[unnamed_idx_1 + R->size[0] * info] = 0.0;
      }
    }

    d_st.site = &r_emlrtRSI;
    overflow = ((!(A->size[0] + 1 > A->size[1])) && (A->size[1] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = A->size[0]; info + 1 <= n; info++) {
      d_st.site = &s_emlrtRSI;
      for (unnamed_idx_1 = 0; unnamed_idx_1 + 1 <= m; unnamed_idx_1++) {
        R->data[unnamed_idx_1 + R->size[0] * info] = b_A->data[unnamed_idx_1 +
          b_A->size[0] * info];
      }
    }

    d_st.site = &t_emlrtRSI;
    e_st.site = &ub_emlrtRSI;
    f_st.site = &vb_emlrtRSI;
    if (!((b_A->size[0] == 0) || (b_A->size[1] == 0))) {
      g_st.site = &qb_emlrtRSI;
      h_st.site = &hb_emlrtRSI;
      g_st.site = &rb_emlrtRSI;
      h_st.site = &hb_emlrtRSI;
      g_st.site = &sb_emlrtRSI;
      h_st.site = &mb_emlrtRSI;
      info_t = LAPACKE_dorgqr(102, (ptrdiff_t)A->size[0], (ptrdiff_t)A->size[0],
        (ptrdiff_t)A->size[0], &b_A->data[0], (ptrdiff_t)A->size[0], &tau->data
        [0]);
      info = (int32_T)info_t;
      g_st.site = &tb_emlrtRSI;
      h_st.site = &nb_emlrtRSI;
      if (info != 0) {
        overflow = true;
        p = false;
        if (info == -7) {
          p = true;
        } else {
          if (info == -5) {
            p = true;
          }
        }

        if (!p) {
          if (info == -1010) {
            h_st.site = &ob_emlrtRSI;
            i_st.site = &ob_emlrtRSI;
            error(&i_st);
          } else {
            h_st.site = &pb_emlrtRSI;
            i_st.site = &pb_emlrtRSI;
            c_error(&i_st, info);
          }
        }
      } else {
        overflow = false;
      }

      if (overflow) {
        i0 = b_A->size[0] * b_A->size[1];
        emxEnsureCapacity_real_T(&f_st, b_A, i0, &b_emlrtRTEI);
        loop_ub = b_A->size[1];
        for (i0 = 0; i0 < loop_ub; i0++) {
          info = b_A->size[0];
          for (unnamed_idx_1 = 0; unnamed_idx_1 < info; unnamed_idx_1++) {
            b_A->data[unnamed_idx_1 + b_A->size[0] * i0] = rtNaN;
          }
        }
      }
    }

    d_st.site = &u_emlrtRSI;
    overflow = ((!(1 > A->size[0])) && (A->size[0] > 2147483646));
    if (overflow) {
      e_st.site = &w_emlrtRSI;
      f_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (info = 0; info + 1 <= m; info++) {
      d_st.site = &v_emlrtRSI;
      for (unnamed_idx_1 = 0; unnamed_idx_1 + 1 <= m; unnamed_idx_1++) {
        Q->data[unnamed_idx_1 + Q->size[0] * info] = b_A->data[unnamed_idx_1 +
          b_A->size[0] * info];
      }
    }

    emxFree_real_T(&b_A);
  }

  emxFree_real_T(&tau);
  i0 = E->size[0] * E->size[1];
  E->size[0] = A->size[1];
  E->size[1] = A->size[1];
  emxEnsureCapacity_real_T(&b_st, E, i0, &b_emlrtRTEI);
  loop_ub = A->size[1] * A->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    E->data[i0] = 0.0;
  }

  for (info = 0; info < A->size[1]; info++) {
    E->data[(jpvt->data[info] + E->size[0] * info) - 1] = 1.0;
  }

  emxFree_int32_T(&jpvt);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (qrGPU.cpp) */
