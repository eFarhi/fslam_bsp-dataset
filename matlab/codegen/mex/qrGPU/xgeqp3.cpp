/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * xgeqp3.cpp
 *
 * Code generation for function 'xgeqp3'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qrGPU.h"
#include "xgeqp3.h"
#include "qrGPU_emxutil.h"
#include "error.h"
#include "eml_int_forloop_overflow_check.h"
#include "qrGPU_data.h"
#include "lapacke.h"

/* Variable Definitions */
static emlrtRSInfo x_emlrtRSI = { 14,  /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 37,  /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 38, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 41, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo cb_emlrtRSI = { 45, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 64, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 67, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 76, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo gb_emlrtRSI = { 79, /* lineNo */
  "xgeqp3",                            /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pathName */
};

static emlrtRSInfo ib_emlrtRSI = { 25, /* lineNo */
  "colon",                             /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

static emlrtRSInfo jb_emlrtRSI = { 78, /* lineNo */
  "colon",                             /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

static emlrtRSInfo kb_emlrtRSI = { 121,/* lineNo */
  "colon",                             /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

static emlrtRSInfo lb_emlrtRSI = { 149,/* lineNo */
  "colon",                             /* fcnName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

static emlrtRTEInfo c_emlrtRTEI = { 1, /* lineNo */
  25,                                  /* colNo */
  "xgeqp3",                            /* fName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 14,/* lineNo */
  5,                                   /* colNo */
  "xgeqp3",                            /* fName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pName */
};

static emlrtRTEInfo f_emlrtRTEI = { 121,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/lib/matlab/ops/colon.m"/* pName */
};

static emlrtRTEInfo g_emlrtRTEI = { 45,/* lineNo */
  5,                                   /* colNo */
  "xgeqp3",                            /* fName */
  "/Applications/MATLAB_R2017b.app/toolbox/eml/eml/+coder/+internal/+lapack/xgeqp3.m"/* pName */
};

/* Function Definitions */
void xgeqp3(const emlrtStack *sp, emxArray_real_T *A, emxArray_real_T *tau,
            emxArray_int32_T *jpvt)
{
  int32_T m;
  int32_T n;
  emxArray_ptrdiff_t *jpvt_t;
  int32_T i1;
  ptrdiff_t m_t;
  int32_T loop_ub;
  ptrdiff_t info_t;
  boolean_T p;
  boolean_T b_p;
  int32_T i2;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &x_emlrtRSI;
  m = A->size[0];
  n = A->size[1];
  b_st.site = &y_emlrtRSI;
  c_st.site = &hb_emlrtRSI;
  b_st.site = &ab_emlrtRSI;
  if ((A->size[0] == 0) || (A->size[1] == 0)) {
    i1 = tau->size[0];
    tau->size[0] = 0;
    emxEnsureCapacity_real_T1(&st, tau, i1, &c_emlrtRTEI);
    b_st.site = &bb_emlrtRSI;
    c_st.site = &ib_emlrtRSI;
    d_st.site = &jb_emlrtRSI;
    e_st.site = &kb_emlrtRSI;
    if (A->size[1] < 1) {
      n = 0;
    } else {
      n = A->size[1];
    }

    i1 = jpvt->size[0] * jpvt->size[1];
    jpvt->size[0] = 1;
    jpvt->size[1] = n;
    emxEnsureCapacity_int32_T(&e_st, jpvt, i1, &f_emlrtRTEI);
    if (n > 0) {
      jpvt->data[0] = 1;
      m = 1;
      f_st.site = &lb_emlrtRSI;
      if ((!(2 > n)) && (n > 2147483646)) {
        g_st.site = &w_emlrtRSI;
        h_st.site = &w_emlrtRSI;
        check_forloop_overflow_error(&h_st);
      }

      for (loop_ub = 2; loop_ub <= n; loop_ub++) {
        m++;
        jpvt->data[loop_ub - 1] = m;
      }
    }
  } else {
    emxInit_ptrdiff_t(&st, &jpvt_t, 1, &g_emlrtRTEI, true);
    i1 = tau->size[0];
    tau->size[0] = muIntScalarMin_sint32(m, n);
    emxEnsureCapacity_real_T1(&st, tau, i1, &e_emlrtRTEI);
    b_st.site = &cb_emlrtRSI;
    c_st.site = &hb_emlrtRSI;
    i1 = jpvt_t->size[0];
    jpvt_t->size[0] = A->size[1];
    emxEnsureCapacity_ptrdiff_t(&st, jpvt_t, i1, &c_emlrtRTEI);
    m = A->size[1];
    for (i1 = 0; i1 < m; i1++) {
      jpvt_t->data[i1] = (ptrdiff_t)0;
    }

    b_st.site = &db_emlrtRSI;
    c_st.site = &hb_emlrtRSI;
    m_t = (ptrdiff_t)A->size[0];
    b_st.site = &eb_emlrtRSI;
    c_st.site = &mb_emlrtRSI;
    info_t = LAPACKE_dgeqp3(102, m_t, (ptrdiff_t)A->size[1], &A->data[0], m_t,
      &jpvt_t->data[0], &tau->data[0]);
    m = (int32_T)info_t;
    b_st.site = &fb_emlrtRSI;
    c_st.site = &nb_emlrtRSI;
    if (m != 0) {
      p = true;
      b_p = false;
      if (m == -4) {
        b_p = true;
      }

      if (!b_p) {
        if (m == -1010) {
          c_st.site = &ob_emlrtRSI;
          d_st.site = &ob_emlrtRSI;
          error(&d_st);
        } else {
          c_st.site = &pb_emlrtRSI;
          d_st.site = &pb_emlrtRSI;
          b_error(&d_st, m);
        }
      }
    } else {
      p = false;
    }

    if (p) {
      i1 = A->size[0] * A->size[1];
      emxEnsureCapacity_real_T(&st, A, i1, &c_emlrtRTEI);
      m = A->size[1];
      for (i1 = 0; i1 < m; i1++) {
        loop_ub = A->size[0];
        for (i2 = 0; i2 < loop_ub; i2++) {
          A->data[i2 + A->size[0] * i1] = rtNaN;
        }
      }

      m = tau->size[0];
      i1 = tau->size[0];
      tau->size[0] = m;
      emxEnsureCapacity_real_T1(&st, tau, i1, &c_emlrtRTEI);
      for (i1 = 0; i1 < m; i1++) {
        tau->data[i1] = rtNaN;
      }

      b_st.site = &gb_emlrtRSI;
      c_st.site = &ib_emlrtRSI;
      d_st.site = &jb_emlrtRSI;
      e_st.site = &kb_emlrtRSI;
      if (n < 1) {
        n = 0;
      }

      i1 = jpvt->size[0] * jpvt->size[1];
      jpvt->size[0] = 1;
      jpvt->size[1] = n;
      emxEnsureCapacity_int32_T(&e_st, jpvt, i1, &f_emlrtRTEI);
      if (n > 0) {
        jpvt->data[0] = 1;
        m = 1;
        f_st.site = &lb_emlrtRSI;
        if ((!(2 > n)) && (n > 2147483646)) {
          g_st.site = &w_emlrtRSI;
          h_st.site = &w_emlrtRSI;
          check_forloop_overflow_error(&h_st);
        }

        for (loop_ub = 2; loop_ub <= n; loop_ub++) {
          m++;
          jpvt->data[loop_ub - 1] = m;
        }
      }
    } else {
      i1 = jpvt->size[0] * jpvt->size[1];
      jpvt->size[0] = 1;
      jpvt->size[1] = jpvt_t->size[0];
      emxEnsureCapacity_int32_T(&st, jpvt, i1, &c_emlrtRTEI);
      m = jpvt_t->size[0];
      for (i1 = 0; i1 < m; i1++) {
        jpvt->data[jpvt->size[0] * i1] = (int32_T)jpvt_t->data[i1];
      }
    }

    emxFree_ptrdiff_t(&jpvt_t);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (xgeqp3.cpp) */
