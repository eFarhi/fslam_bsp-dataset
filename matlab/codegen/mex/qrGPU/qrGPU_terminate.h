/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * qrGPU_terminate.h
 *
 * Code generation for function 'qrGPU_terminate'
 *
 */

#ifndef QRGPU_TERMINATE_H
#define QRGPU_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "qrGPU_types.h"

/* Function Declarations */
extern void qrGPU_atexit();
extern void qrGPU_terminate();

#endif

/* End of code generation (qrGPU_terminate.h) */
