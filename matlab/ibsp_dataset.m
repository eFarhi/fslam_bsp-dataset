classdef ibsp_dataset
    %DATASET Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Robot = struct('RotationMat',[],'InitialPos',[])
        OptionalPayloads = struct('Mono_Camera','Mono_Camera','Lidar','Lidar',...
            'Stereo_Camera','Stereo_Camera','IMU','IMU'...
            )
        Payloads = struct('Mono_Camera',struct('calib_mat',[],'Path',[]),...
            'Lidar',struct('spec',[],'Path',[]),...
            'Stereo_Camera',struct('calib_mat',[],'Path',[]),...
            'IMU',struct('spec',[],'Path',[])...
            )
        GroundTruth = struct('trajectory',[],'data',[],'Path','Dataset/GT/data')
        DA = struct('Correspondence_mat',[])
        dataset_path = 'Dataset/'
    end
    
    methods
        %% init dataset
        function [dataset] = load_Robot(dataset,path)
            
        end
        
        function [dataset] = load_GroundTruth(dataset,path)
            
        end
        %% dataset options
        function [odom] = get_odom(dataset,pose_index)
            odom = 1;
        end
        
    end
    
end

