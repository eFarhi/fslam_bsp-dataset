function [next_manuever,PlanningOutput,AdditionalOutput] = ibsp_ds06_Planning(DS,DA,keys,RobotSettings,FLAGS,pose_index,belief)
%% Planning meant to provide with the next optimal action


%% ############# Output Doc ##############################################
% AdditionalOutput.
%   @. manuever_length - horizon of the manuever
%   @. sequence_score - score of this action sequence (meaningless)
%   @. curr_pose - gtsam.pose3 - pose at time k|k
%   @. visible_landmarks - index of visible landmarks up to time k|k
%   @. score - vector - score of each action in the horizon
%   @. manuever{horizon step}.
%       @.@{}. planned odometry - gtsam.pose3 - control command
%       @.@{}. pose - gtsam.pose3 - pose value at this horizon step
%       @.@{}. belief.
%           @.@.{}.@ newFactors - gtsam.nonlinearfg - new factors added at this horizon step
%           @.@.{}.@ newValues - gtsam.values - new values added at this horizon step
%           @.@.{}.@ belief - gtsam.isam2 - belief k+l|k
%           @.@.{}.@ estimate - gtsam.values - belief k+l|k, solution

% ########################################################################


%% init
cost_or_reward = 1; % 0- cost, 1- reward
main_sequence_length = 1; % horizon length of the chosen action sequence
if pose_index ==2501
    next_manuever = [];
    AdditionalOutput = [];
    return
end
%% Create possible action sequences
for i = 1 : main_sequence_length
    candidate_manuever{1}{i} = DS.GroundTruth.trajectory{pose_index + i -1};
end
% candidate_manuever{2}{1} = gtsam.Pose3(gtsam.Rot3(eye(3)),gtsam.Point3(zeros(3,1)));

candidate_manuever_num = length(candidate_manuever);
%% get current landmarks
if isfield(keys,'landmark_keys_vec')
    landmark_keys = keys.landmark_keys_vec;
    landmark_index = find(landmark_keys > 0);
    landmark_keys = landmark_keys(landmark_index);
end
%% done for each action sequence
inf_pose_key = keys.pose_keys_vec(pose_index-1);
curr_pose = DA.values.at(inf_pose_key);
for i = 1 : candidate_manuever_num
    % init
    output{i}.manuever_length = length(candidate_manuever{i});
    output{i}.sequence_score = 0;
    % extract the last pose
    output{i}.curr_pose = curr_pose;
    % calc manuevers
    for j = 1 : output{i}.manuever_length
        % init factors and values
        output{i}.manuever{j}.belief.newFactors = gtsam.NonlinearFactorGraph;
        proj_factors = gtsam.NonlinearFactorGraph;
        output{i}.manuever{j}.belief.newValues = gtsam.Values;
        %% propogate the step
            % create the key
            pose_key = gtsam.symbol('x',pose_index+j-1);
            % corrupt the odometry command with model noise
            noise = mvnrnd(zeros(size(RobotSettings.odometry_sigma)), RobotSettings.odometry_sigma.^2)';
            planned_odometry = candidate_manuever{i}{j}.retract(noise);
            output{i}.manuever{j}.planned_odometry = planned_odometry;
            output{i}.manuever{j}.planned_odometry = candidate_manuever{i}{j};
            % concatenate the planned odometry to last pose
            if j == 1
                % use last estimated pose from inference
                output{i}.manuever{j}.pose = output{i}.curr_pose.compose(planned_odometry);
                odom_fac = gtsam.BetweenFactorPose3(inf_pose_key, pose_key, candidate_manuever{i}{j}, RobotSettings.odometry_noise_model);
            else
                % inside the planning horizon, use last planned pose
                output{i}.manuever{j}.pose = output{i}.manuever{j-1}.pose.compose(planned_odometry);
                pose_prev_key = gtsam.symbol('x',pose_index+j-2);
                odom_fac = gtsam.BetweenFactorPose3(pose_prev_key, pose_key, candidate_manuever{i}{j}, RobotSettings.odometry_noise_model);
            end
            % create motion factor and value
            output{i}.manuever{j}.belief.newFactors.push_back(odom_fac);
            output{i}.manuever{j}.belief.newValues.insert(pose_key, output{i}.manuever{j}.pose);
        %% which previously seen landmark will we observed
        output{i}.visible_landmarks = [];
           if isfield(keys,'landmark_keys_vec')
               T_g2c = output{i}.manuever{j}.pose.matrix;
               T_g2c(1:3,1:3) = T_g2c(1:3,1:3)';
               T_g2c(1:3,4) = -T_g2c(1:3,1:3)*T_g2c(1:3,4);
               for l_index = 1:length(landmark_keys)
                   % check if should be seen in time k+l
                   landmark_seen_in_kl = DS.DA.Correspondence_mat(landmark_index(l_index),pose_index+j-1) > 0;
                   if landmark_seen_in_kl 
                       % get landmark in global frame
                       landmark_g = DA.values.at(landmark_keys(l_index)).vector;
                       % transform the landmark to current camera frame
                       landmark_c = T_g2c*[landmark_g;1];
                       landmark_c(end) = [];
                       % check distance
                       visible_flag = norm(landmark_c) < RobotSettings.Camera.MaximumObservRange;
                       % check if infront of camera
                       visible_flag = visible_flag && landmark_c(end) > 0;
                       % check if in frame
                       if visible_flag
                           % extract pixels
                           K = DA.Payloads.Mono_Camera.calib_mat.matrix;
                           landmark_2d = K*landmark_c;
                           landmark_2d = landmark_2d./landmark_2d(end);
                           landmark_2d(end) = [];
                           visible_flag = 0 < landmark_2d(1) && landmark_2d(1) < RobotSettings.Camera.image_width;
                           visible_flag = visible_flag && 0 < landmark_2d(2) && landmark_2d(2) < RobotSettings.Camera.image_hieght;
                           if visible_flag
                               % add landmark index to output
                               output{i}.visible_landmarks = [output{i}.visible_landmarks; landmark_index(l_index)];
                               % create factor
                               pixels = gtsam.Point2(landmark_2d);
                               l_key = landmark_keys(l_index);
                               proj_fac = gtsam.GenericProjectionFactorCal3_S2(pixels, DA.Payloads.Mono_Camera.noise_model, pose_key, l_key,DA.Payloads.Mono_Camera.calib_mat);
                               output{i}.manuever{j}.belief.newFactors.push_back(proj_fac);
                               proj_factors.push_back(proj_fac);
                           end
                       end
                   end
               end
           end
        %% solve the belief
        % get b(X_k|k)
        if j==1
            output{i}.manuever{j}.belief.belief = gtsam.ISAM2(belief);
        else
            output{i}.manuever{j}.belief.belief = gtsam.ISAM2(output{i}.manuever{j-1}.belief.belief);
        end
        
        if output{i}.manuever{j}.belief.newFactors.size() > 0
            output{i}.manuever{j}.belief.belief.update(output{i}.manuever{j}.belief.newFactors, output{i}.manuever{j}.belief.newValues);
%             for s=1:5
%                 output{i}.manuever{j}.belief.belief.update;
%             end
            output{i}.manuever{j}.belief.estimate = output{i}.manuever{j}.belief.belief.calculateBestEstimate();
        end
        %% calc objective
        % current step score - sequense {1} would always win!!
        output{i}.score(j) = cost_or_reward*(1/i) + (1-cost_or_reward)*(i-1);
        % cum score
        output{i}.sequence_score = output{i}.sequence_score + output{i}.score(j);
    end
    cum_scores(i) = output{i}.sequence_score;
end
%% calculate the best next action
if cost_or_reward
    [~,best_index] = max(cum_scores);
else
    [~,best_index] = min(cum_scores);
end
%% returning the next optimal action
next_manuever = candidate_manuever{best_index};

%% returning additional output
AdditionalOutput = output{best_index};

%% returning planning output for ibsp

PlanningOutput.belief_k_k = belief;

PlanningOutput.belief_k1_k = AdditionalOutput.manuever{1}.belief.belief;
PlanningOutput.newFactors_k1_k = AdditionalOutput.manuever{1}.belief.newFactors;
PlanningOutput.newValues_k1_k = AdditionalOutput.manuever{1}.belief.newValues;
PlanningOutput.estimate = AdditionalOutput.manuever{1}.belief.estimate;

% PlanningOutput.Q_k1_k_mot = [];     %AdditionalOutput.manuever{1}.belief.Q_k1_k_mot
% PlanningOutput.Q_k1_k = [];         %AdditionalOutput.manuever{1}.belief.Q_k1_k
% PlanningOutput.R_k1_k_mot = [];     %AdditionalOutput.manuever{1}.belief.R_k1_k_mot
% PlanningOutput.R_k1_k = [];         %AdditionalOutput.manuever{1}.belief.R_k1_k
% PlanningOutput.d_k1_k_mot = [];     %AdditionalOutput.manuever{1}.belief.d_k1_k_mot
% PlanningOutput.d_k1_k = [];         %AdditionalOutput.manuever{1}.belief.d_k1_k

end