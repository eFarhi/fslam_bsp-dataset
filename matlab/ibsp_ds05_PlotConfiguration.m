function [fig_config] = ibsp_ds05_PlotConfiguration(FLAGS,p_to_saving_results)
%% IBSP05_PlotConfiguration sets up the plot configurations
%
%
%% What 2 save & what 2 plot
fig_config.save.all_figs = true;
fig_config.save.for_ext_mov = true;
fig_config.plot.plot_cov   = 0;
fig_config.plot.cov_delta = 5;
fig_config.plot.plot_current_cov = 1; % 1: current; 0: final
fig_config.plot.show_incremental_covs = 1;
fig_config.plot.plot_flag  = 0;
fig_config.plot.landmark_tag = 0;

%% Folder location
p_to_save_figs  = [];
p_to_anim_files = [];

if fig_config.save.all_figs
    if ~FLAGS.upload
        if exist([p_to_saving_results filesep 'FIGS'],'dir')
            error('FIGS folder already exists!');
        end
        mkdir(p_to_saving_results, 'FIGS');
    end
    p_to_save_figs  = [p_to_saving_results 'FIGS' filesep];
end

if fig_config.save.for_ext_mov
     if ~FLAGS.upload
         if exist([p_to_saving_results filesep 'movie'],'dir')
             error('Movie folder already exists!');
         end
         mkdir(p_to_saving_results, 'movie');
     end
    p_to_anim_files  = [p_to_saving_results 'movie' filesep];
end

fig_config.path.p_to_anim_files = p_to_anim_files;
fig_config.path.p_to_save_figs = p_to_save_figs;


%% Design Settings
fig_config.design.fontsize = 20;
fig_config.design.marker.landmark_isam = '+';
fig_config.design.marker.landmark_ibsp = 'o';
fig_config.design.MappedLandmark = [0 0.498, 0]; % green
fig_config.design.CurrObservedLandmark = 'magenta';
fig_config.design.RobotPose_Col_est   = 'red';
fig_config.design.RobotPose_Col_est_ibsp   = 'black';
fig_config.design.RobotPose_Col_true  = 'blue';
fig_config.design.Markersize = 5;
fig_config.design.Linewidth  = 2;

%% labels
fig_config.label.y = 'z_{[m]}'; 
fig_config.label.x = 'x_{[m]}';
fig_config.label.z = 'y_{[m]}';

%% Map settings
fig_config.map.use_real_map = 0;
fig_config.map.Boundary.S_N = [-300 400];
fig_config.map.Boundary.W_E = [-300 400];
fig_config.map.Boundary.Down = [-30 30];


end

