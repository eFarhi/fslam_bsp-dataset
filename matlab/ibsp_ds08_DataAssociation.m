function [ DA,values,factors,keys,output ] = ibsp_ds08_DataAssociation( DA, CurrentReading , current_pose,values,factors,keys,RobotSettings)
%IBSP_DS08_DATAASSOCIATION
switch CurrentReading.Payload
    case DA.OptionalPayloads.Mono_Camera
        %% Storing the information
        DA.Payloads.Mono_Camera.data{current_pose} = CurrentReading.Measurements;
        %% locating descriptor matching
        if isempty(CurrentReading.Correspondence)
%             index_vec = ibsp_ds11_DescriptorMatching();
%             DA = bla_bla;
        else
            DA.Payloads.Mono_Camera.Correspondence_Mat(:,current_pose,1) = CurrentReading.Correspondence;
            index = DA.Payloads.Mono_Camera.Correspondence_Mat(:,current_pose,1) > 0;
            DA.Payloads.Mono_Camera.Correspondence_Mat(index,current_pose,2) = 1;
            DA.Payloads.Mono_Camera.Descriptors(:,index) = CurrentReading.Descriptor(:,DA.Payloads.Mono_Camera.Correspondence_Mat(index,current_pose,1));
        end
        %% Check if a Landmark is required to be triangulated
        % take only features sighted now
        feature_index = find(DA.Payloads.Mono_Camera.Correspondence_Mat(:,current_pose,2) > 0) ;
        % take only features sighted more than once
        feature_occurrences = sum(DA.Payloads.Mono_Camera.Correspondence_Mat(feature_index,1:current_pose,2),2) > 1;
        feature_occurrences = feature_index(feature_occurrences);
        % mark features that are already landmarks
        if isfield(keys,'landmark_keys_vec')
            landmark_index = find(keys.landmark_keys_vec > 0);
            existing_landmarks = feature_occurrences(ismember(feature_occurrences,landmark_index)) ;
        else
            existing_landmarks = [];
        end
        %% Create Factors for existing landmarks
        for i = existing_landmarks'
            landmark_data_position = DA.Payloads.Mono_Camera.Correspondence_Mat(i,current_pose,1);
            pixels = gtsam.Point2(DA.Payloads.Mono_Camera.data{current_pose}(1:2,landmark_data_position));
            pose_key = keys.pose_keys_vec(current_pose); %gtsam.symbol('x',current_pose);
            l_key = keys.landmark_keys_vec(i); %gtsam.symbol('l',i);
            proj_fac = gtsam.GenericProjectionFactorCal3_S2(pixels, DA.Payloads.Mono_Camera.noise_model, pose_key, l_key, DA.Payloads.Mono_Camera.calib_mat);
            factors.predicted.push_back(proj_fac);
            factors.all.push_back(proj_fac);
            DA.Payloads.Mono_Camera.Correspondence_Mat(i,current_pose,2) = 3; % document it was used as factor
            % disclude this measurement from landmark required triangulation list -feature_occurrences
            feature_occurrences(feature_occurrences == i) = [];
        end
        if isempty(feature_occurrences) 
            % not enough features to triangulate new landmarks
        else
            %% Triangulate new Landmark
           [DA,values,factors,keys,Trian_output] = ibsp_ds12_TriangulateLandmark(DA,feature_occurrences,current_pose,values,factors,CurrentReading.Payload,keys,RobotSettings);            
           output.new_landmarks_num = Trian_output.new_landmarks_num;
           output.fac_of_new_landmarks = Trian_output.fac_of_new_landmarks;
        end
        output.existing_landmarks_num = length(existing_landmarks);
    case DA.OptionalPayloads.Lidar
        
    case DA.OptionalPayloads.Stereo_Camera
        
    otherwise
        warning('No such payload is known by DA, no action has been taken')
end 
        
end

