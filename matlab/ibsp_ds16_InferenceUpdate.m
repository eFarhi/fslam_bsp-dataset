function [isam_belief, ibsp_belief,InfUpdateOutput] = ibsp_ds16_InferenceUpdate( isam_params,belief,newFactors,newValues,PlanningOutput,compare,rep_calc,R_k_k,E_k_k,d_k_k,pose_index)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% init
method = [3 5]; %1-OTM, 2- DU, 3- OTM-OO, 4-DU-OO, 5- iSAM, 6- Batch
bulk = 5000;
%% Retrieve R_k_k
if isempty(R_k_k)
    fg_k_k = belief.getFactorsUnsafe;
    val_k_k = belief.calculateBestEstimate();
    l_fg_k_k = fg_k_k.linearize(val_k_k);
    [A_k_k,b_k_k] = BuildLarge_A_and_b(fg_k_k,l_fg_k_k,pose_index-1);
try
    [Q_k_k,R_k_k,E_k_k] = qr(A_k_k);
    Q_k_k(:,diag(R_k_k)<0) = -Q_k_k(:,diag(R_k_k)<0);
    R_k_k(diag(R_k_k)<0,:) = -R_k_k(diag(R_k_k)<0,:);
    d_k_k = Q_k_k'*b_k_k; 
    d_k_k( ~any(R_k_k,2)) = [];  %remove zero rows
    R_k_k( ~any(R_k_k,2), : ) = [];  %remove zero rows
catch
    [R_k_k,d_k_k] = infup010_HouseHolder(A_k_k,b_k_k);
    E_k_k = sparse(eye(length(R_k_k(1,:))));
end
    clear l_fg_k_k Q_k_k A_k_k
end
%% Extract b k+1|k+1
fg_k_k = belief.getFactorsUnsafe;
fg_k1_k1 = fg_k_k.clone;
val_k_k = belief.calculateBestEstimate;
values_k1_k1 =gtsam.Values;
values_k1_k1.insert(newValues);
values_k1_k1.insert(val_k_k);
fg_k1_k1.push_back(newFactors);

lfg_k1_k1 = fg_k1_k1.linearize(values_k1_k1);
[~,b_k1_k1] = BuildLarge_A_and_b(fg_k1_k1,lfg_k1_k1,pose_index);

clear fg_k1_k1 values_k1_k1 lfg_k1_k1
%% Extract R k+1|k & d k+1|k
% init factors
fg_k1_k = fg_k_k.clone;
fg_k1_k_mot = fg_k_k.clone;
fg_k1_obs = gtsam.NonlinearFactorGraph;
values_k1_k =gtsam.Values;
values_k1_k.insert(val_k_k);
values_k1_k.insert(newValues); %PlanningOutput.belief_k1_k.getLinearizationPoint);
% devide factors according to types
newFactors_k1_class_array = [];
for i = 0 : newFactors.size-1 %PlanningOutput.newFactors_k1_k.size-1
    fac = newFactors.at(i); %PlanningOutput.newFactors_k1_k.at(i);
    fac_class = class(fac);
    % ######### number is what you need to add for a new factor type #########
    %             lfac = fac.linearize(values_k1_k);
    %             A = lfac.augmentedJacobian;
    %             number = length(A(:,1));
    % ########################################################################
    switch fac_class
        case 'gtsam.BetweenFactorPose3'
            newFactors_k1_class_array(i+1,1) = 6;
            fg_k1_k.push_back(fac);
            fg_k1_k_mot.push_back(fac);
        case 'gtsam.GenericProjectionFactorCal3_S2'
            newFactors_k1_class_array(i+1,1) = 2;
            fg_k1_k.push_back(fac);
            fg_k1_obs.push_back(fac);
        case 'gtsam.RangeFactorPosePoint3'
            newFactors_k1_class_array(i+1,1) = 3;
            fg_k1_k.push_back(fac);
            fg_k1_obs.push_back(fac);
        case 'gtsam.PriorFactorPose3'
            newFactors_k1_class_array(i+1,1) = 6;
            fg_k1_k.push_back(fac);
            fg_k1_k_mot.push_back(fac);
        case 'gtsam.PriorFactorPoint3'
            newFactors_k1_class_array(i+1,1) = 3;
            fg_k1_k.push_back(fac);
            fg_k1_obs.push_back(fac);
        otherwise
            error('########## Add case of class: %s to the switch in ibsp_ds16_InferenceUpdate.m ##########',fac_class)
    end
end
newFactors_k1_size = sum(newFactors_k1_class_array);
lfg_k1_k = fg_k1_k.linearize(values_k1_k);
[A_k1_k,b_k1_k] = BuildLarge_A_and_b(fg_k1_k,lfg_k1_k,pose_index);
b_k1_k = [d_k_k ; b_k1_k(end -newFactors_k1_size+1 : end)];

aug_A_k_k = A_k1_k(1: end -newFactors_k1_size, :); % get R_k_k augmented with zero pedding for new variables
new_var_num_k1 = length(aug_A_k_k(1,:)) - length(R_k_k(1,:));
aug_E_k_k = E_k_k;
aug_E_k_k(end+1:end+new_var_num_k1,end+1:end+new_var_num_k1) = eye(new_var_num_k1);
aug_R_k_k = R_k_k;
aug_R_k_k(end,end+new_var_num_k1) = 0;

A_k1 = A_k1_k(end - newFactors_k1_size +1:end, :);
b_k1 = b_k1_k(end - newFactors_k1_size +1:end, :);
b_k1_real = b_k1_k1(end - newFactors_k1_size +1:end);
if fg_k1_k_mot.size > fg_k_k.size
    A_k1_k_mot = A_k1_k(1: end - (newFactors_k1_size - newFactors_k1_class_array(1)),:);
    A_k1_mot = A_k1_k_mot(end - newFactors_k1_class_array(1)+1:end,:);
    b_k1_k_mot = b_k1_k(1: end - (newFactors_k1_size - newFactors_k1_class_array(1)),:);
    b_k1_mot = b_k1_k_mot(end - newFactors_k1_class_array(1)+1:end,:);
    A_k1_obs = A_k1(newFactors_k1_class_array(1)+1:end,:);
    b_k1_obs = b_k1(newFactors_k1_class_array(1)+1:end);
    b_k1_obs_real = b_k1_real(newFactors_k1_class_array(1)+1:end);
else
%     A_k1_k_mot = aug_A_k_k;
    b_k1_k_mot = b_k_k;
    A_k1_obs = A_k1;
    b_k1_obs = b_k1;
    b_k1_obs_real = b_k1_real;
end

clear A_k1_k_mot    newFactors_k1_class_array    A_k1_k    ...
    b_k1_k    fg_k1_k_mot    aug_A_k_k    b_k_k    
% Enable if using method 2 or 4
clear R_k_k   E_k_k

% factorize R_k_k & A_k1
[Q_k1_k,R_k1_k, E_k1_k] = qr([aug_R_k_k;A_k1*aug_E_k_k]); % E_k1_k is the new ordering
Q_k1_k(:,diag(R_k1_k)<0) = -Q_k1_k(:,diag(R_k1_k)<0);
R_k1_k(diag(R_k1_k)<0,:) = -R_k1_k(diag(R_k1_k)<0,:);
% d_k1_k = Q_k1_k'*[d_k_k;b_k1];

% #################### handling large matrix #######################
    % breaking down the matrix
%         bulk = 10000;
        counter = 1;
        leng = length(Q_k1_k(:,1));
        while counter*bulk <= leng
            Q_k1_k_large{counter} = Q_k1_k(:,1+(counter-1)*bulk:counter*bulk);
            counter = counter+1;
        end
        counter = counter-1;
        Q_k1_k_length = counter;
        if counter*bulk < leng
            Q_k1_k_large{counter+1} = Q_k1_k(:,1+counter*bulk:end);
            Q_k1_k_length = counter+1;
        end
        clear Q_k1_k
    % solving
        d_k1_k = [];
        for j = 1:length(Q_k1_k_large)
            d_k1_k = [d_k1_k ; Q_k1_k_large{j}'*[d_k_k;b_k1]];
        end
% ##################################################################
d_k1_k( ~any(R_k1_k,2)) = [];  %remove zero rows
R_k1_k( ~any(R_k1_k,2), : ) = [];  %remove zero rows

% factorize R_k_k & A_k1_mot
[Q_k1_k_mot,R_k1_k_mot,E_k1_k_mot] = qr([aug_R_k_k;A_k1_mot*aug_E_k_k]);
Q_k1_k_mot(:,diag(R_k1_k_mot)<0) = -Q_k1_k_mot(:,diag(R_k1_k_mot)<0);
R_k1_k_mot(diag(R_k1_k_mot)<0,:) = -R_k1_k_mot(diag(R_k1_k_mot)<0,:);

% d_k1_k_mot = Q_k1_k_mot'*b_k1_k_mot;
% #################### handling large matrix #######################
    % breaking down the matrix
%         bulk = 10000;
        counter = 1;
        leng = length(Q_k1_k_mot(:,1));
        while counter*bulk <= leng
            Q_k1_k_mot_large{counter} = Q_k1_k_mot(:,1+(counter-1)*bulk:counter*bulk);
            counter = counter+1;
        end
        counter = counter-1;
        Q_k1_k_mot_length = counter;
        if counter*bulk < leng
            Q_k1_k_mot_large{counter+1} = Q_k1_k_mot(:,1+counter*bulk:end);
            Q_k1_k_mot_length = counter +1;
        end
        clear Q_k1_k_mot
    % solving
        d_k1_k_mot = [];
        for j = 1:length(Q_k1_k_mot_large)
            d_k1_k_mot = [d_k1_k_mot ; Q_k1_k_mot_large{j}'*[d_k_k;b_k1_mot]];
        end
        clear Q_k1_k_mot_large
% ##################################################################
d_k1_k_mot( ~any(R_k1_k_mot,2)) = [];  %remove zero rows
R_k1_k_mot( ~any(R_k1_k_mot,2), : ) = [];  %remove zero rows


% factorize R_k1_k_mot & A_k1_obs
[Q_k1_k_obs,R_k1_k_obs,E_k1_k_obs] = qr([R_k1_k_mot ; A_k1_obs*E_k1_k_mot]);
Q_k1_k_obs(:,diag(R_k1_k_obs)<0) = -Q_k1_k_obs(:,diag(R_k1_k_obs)<0);
R_k1_k_obs(diag(R_k1_k_obs)<0,:) = -R_k1_k_obs(diag(R_k1_k_obs)<0,:);
R_k1_k_obs( ~any(R_k1_k_obs,2), : ) = [];  %remove zero rows
% #################### handling large matrix #######################
    % breaking down the matrix
%         bulk = 10000;
        counter = 1;
        leng = length(Q_k1_k_obs(:,1));
        while counter*bulk <= leng
            Q_k1_k_obs_large{counter} = Q_k1_k_obs(:,1+(counter-1)*bulk:counter*bulk);
            counter = counter+1;
        end
        counter = counter-1;
        Q_k1_k_obs_length = counter;
        if counter*bulk < leng
            Q_k1_k_obs_large{counter+1} = Q_k1_k_obs(:,1+counter*bulk:end);
            Q_k1_k_obs_length = counter +1;
        end
        clear Q_k1_k_obs
% ##################################################################

%% calc inference update
for i = method
    infup_time = 0;
    switch i
        case 1
            %% Method #1 - using Q
            methods{i} = 'OTM';
            for step = 1:rep_calc
                tic
%                 [R_k1_k1,d_k1_k1] = infup001_AlldataUpdate(d_k_k,b_k1_real,Q_k1_k,R_k1_k);
                [R_k1_k1,d_k1_k1] = infup001_AlldataUpdate_LargeMat(d_k_k,b_k1_real,Q_k1_k_large,R_k1_k);
                timing = toc;
                E = E_k1_k;
                infup_time = infup_time + timing;
            end
            infup_time = infup_time/rep_calc;
        case 2
            %% Method #2 - DownUp
            methods{i} = 'DU';
            for step = 1:rep_calc
                tic
                [R_k1_k1,d_k1_k1] = infup002_AlldataDownUpdate(R_k_k,R_k1_k,d_k_k,A_k1,b_k1_real);
                timing = toc;
                E = E_k1_k;
                infup_time = infup_time + timing;
            end
            infup_time = infup_time/rep_calc;
        case 3
            %% Method #3 - using Q just for measurememts
            methods{i} = 'OTM-OO';
            for step = 1:rep_calc
                tic
%                 [R_k1_k1,d_k1_k1] = infup003_MeasdataUpdate(d_k1_k_mot,R_k1_k,b_k1_obs_real,Q_k1_k_obs);
                [R_k1_k1,d_k1_k1] = infup003_MeasdataUpdate_LargeMat(d_k1_k_mot,R_k1_k,b_k1_obs_real,Q_k1_k_obs_large);
                timing = toc;
                E = E_k1_k_mot*E_k1_k_obs;
                infup_time = infup_time+timing;
            end
            infup_time = infup_time/rep_calc;
        case 4
            %% Method #4 - DownUp just for measurements
            methods{i} = 'DU-OO';
            for step = 1:rep_calc
                tic
                [R_k1_k1,d_k1_k1] = infup004_MeasdataDownUpdate(R_k1_k_mot,R_k1_k,d_k1_k,A_k1_obs,b_k1_obs,b_k1_obs_real);
                timing = toc;
                E =E_k1_k_mot*E_k1_k_obs;
                infup_time = infup_time + timing;
            end
            infup_time = infup_time/rep_calc;
            
        case 5
            %% Method #5 - isam C++ package
            methods{i} = 'iSAM';
            infup_time = 0;
            for step = 1:rep_calc
                isam_params.setEnableRelinearization(false);
                isam_temp = gtsam.ISAM2(isam_params);
                isam_temp.update(belief.getFactorsUnsafe, belief.calculateBestEstimate);
                for rep_update=1:5
                    isam_temp.update();
                end
                if isempty(PlanningOutput)
                    tic
                        result_k1_k1 = isam_temp.update(newFactors,newValues);
                    timing = toc;
                    infup_time = infup_time + timing;
                else
                    % update what has been done in planning
                    tic
                        result_k1_k1 = isam_temp.update(PlanningOutput.Factors_k1_k,PlanningOutput.newValues_k1_k);
                    timing = toc;
                    infup_time = infup_time + timing;
                    if ~compare.match
                        % update what has been added in DA_fix
                        try
                            tic
                                result_k1_k1 = isam_temp.update(PlanningOutput.new_factors_from1,PlanningOutput.add_new_values_k1_k1,PlanningOutput.bad_indx);
                            timing = toc;
                        catch
                            tic
                                result_k1_k1 = isam_temp.update(PlanningOutput.new_factors_from1,PlanningOutput.add_new_values_k1_k1);
                            timing = toc;
                        end
                        infup_time = infup_time + timing;
                    end
                end
            end
            infup_time = infup_time/rep_calc;
        case 6
%             %% Method #6 - batch inference with sparsity
%             methods{i} = 'STD';
%             for step = 1:rep_calc
%                 tic
%                 [R_k1_k1,d_k1_k1,E] = infup011_BatchInference( aug_A_k_k,A_k1,b_k_k,b_k1_real );
%                 timing = toc;
%                 infup_time = infup_time + timing;
%             end
%             infup_time = infup_time/rep_calc;
            disp('STD method is currently inactive')
        otherwise
            disp('no such method, use 1-6')
    end
    if i ==5
        InfUpdateOutput.results{i}.result_k1_k1 = result_k1_k1;
    else
        InfUpdateOutput.results{i}.R_k1_k1 = R_k1_k1;
        InfUpdateOutput.results{i}.d_k1_k1 = d_k1_k1;
        InfUpdateOutput.results{i}.E = E;
    end
    InfUpdateOutput.timing_mat(1,i)=infup_time;
    InfUpdateOutput.results{i}.method_name = methods{i};
end

ibsp_belief = gtsam.ISAM2(belief);
ibsp_belief.update(newFactors,newValues);
isam_belief = gtsam.ISAM2(belief);
isam_belief.update(newFactors, newValues);


end