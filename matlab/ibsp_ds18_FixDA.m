function [PlanningOutput] = ibsp_ds18_FixDA( PlanningOutput,compare,isam_params,rep_calc )
%ibsp_ds18_FixDA
fix_time = 0;
for repeat = 1:rep_calc
    %% init
        isam_params.setEnableRelinearization(false);
        belief_k1_k = gtsam.ISAM2(isam_params);
        belief_k1_k.update(PlanningOutput.belief_k_k.getFactorsUnsafe,PlanningOutput.belief_k_k.calculateBestEstimate);
        for ii = 1:5
            belief_k1_k.update();
        end
        belief_k1_k.update(PlanningOutput.newFactors_k1_k,PlanningOutput.newValues_k1_k);
        for ii = 1:5
            belief_k1_k.update();
        end

    FG_k1_k = belief_k1_k.getFactorsUnsafe;
    bad_indx = gtsam.KeyVector;
    
    %% delete all existing variables from compare.new_values_from1
    values_k1_k = belief_k1_k.getLinearizationPoint; %check if different
    keys_new_var_k1_k1 = gtsam.KeyVector(compare.new_values_from1.keys);
    add_new_values_k1_k1 = gtsam.Values(compare.new_values_from1);
    if compare.new_values_from1.size
        for i = 0 : compare.new_values_from1.size-1
            key = keys_new_var_k1_k1.at(i);
            if values_k1_k.exists(key)
                add_new_values_k1_k1.erase(key);
            end
        end
    else
        add_new_values_k1_k1 = gtsam.Values;
    end
    %% fix DA
    if ~isempty(compare.bad_factors_indxIn2)
        for k = 1 : length(compare.bad_factors_indxIn2)
            bad_indx.push_back(compare.bad_factors_indxIn2(k) + FG_k1_k.size - PlanningOutput.newFactors_k1_k.size);
        end
        tic
        result_k1_k = belief_k1_k.update(compare.new_factors_from1,add_new_values_k1_k1,bad_indx);
        timing = toc;
    else
         tic
        result_k1_k = belief_k1_k.update(compare.new_factors_from1,add_new_values_k1_k1);
        timing = toc;
    end
    fix_time = fix_time+timing;
end %rep end
fix_time = fix_time/rep_calc;
test_fix_time = 0;%test_timing;
%% create updated newFactors_k1_k
values_k1_k = belief_k1_k.getLinearizationPoint;
Factors_k1_k = belief_k1_k.getFactorsUnsafe;
Factors_k_k = PlanningOutput.belief_k_k.getFactorsUnsafe;
newFactors_k1_k = gtsam.NonlinearFactorGraph;
newFactors_k1_k_mes = gtsam.NonlinearFactorGraph;

i = Factors_k_k.size;
while i <=Factors_k1_k.size-1
    if ~Factors_k1_k.exists(i)
        i=i+1;
    else
        if i > Factors_k_k.size
            newFactors_k1_k_mes.push_back(Factors_k1_k.at(i));
        end
        newFactors_k1_k.push_back(Factors_k1_k.at(i));
        i=i+1;
    end
end

%% output data
PlanningOutput.belief_k1_k = belief_k1_k;
PlanningOutput.add_new_values_k1_k1 = add_new_values_k1_k1;
PlanningOutput.new_factors_from1 = compare.new_factors_from1;
PlanningOutput.bad_indx = bad_indx;
PlanningOutput.Factors_k1_k = PlanningOutput.newFactors_k1_k;
PlanningOutput.newFactors_k1_k = newFactors_k1_k;
PlanningOutput.fac_removed = bad_indx.size;
PlanningOutput.fac_added = compare.new_factors_from1.size;
PlanningOutput.fix_time = fix_time;
PlanningOutput.test_fix_time = test_fix_time;
end


