function [landmark_init_value] = triangulate_elad(varargin)
%% triangulate triangulates a landmark, based on measurements from two global poses.
% input e.g. : 
    % landmark_init_value = triangulate_elad(pose_pre,pose_last, pre(1:2),last(1:2),DA.Payloads.Mono_Camera.calib_mat)
%% Variable Input
counter = length(varargin);
switch counter
    case 5
        pose_pre = varargin{1};
        pose_last = varargin{2};
        pre_pix = varargin{3};
        last_pix = varargin{4};
        K_pre = varargin{5};
        K_pre = K_pre.matrix;
        K_last = K_pre;
    case 6
        pose_pre = varargin{1};
        pose_last = varargin{2};
        pre_pix = varargin{3};
        last_pix = varargin{4};
        K_pre = varargin{5};
        K_last = varargin{6};
        K_pre = K_pre.matrix;
        K_last = K_last.matrix;
    otherwise
        error('number of input variables dont match the function')
end
    %% Retrieve transformation 
        R_pre = pose_pre.rotation.matrix;
        t_pre = pose_pre.translation.vector;
        R_pre = R_pre';
        t_pre = -R_pre*t_pre;
        
        R_last = pose_last.rotation.matrix;
        t_last = pose_last.translation.vector;
        R_last = R_last';
        t_last = -R_last*t_last;
    %% Build projection matrices
        M_pre = [K_pre*R_pre K_pre*t_pre];
        M_last = [K_last*R_last K_last*t_last];

    %% Build equations
        A = [pre_pix(1)*M_pre(3,1)-M_pre(1,1) , pre_pix(1)*M_pre(3,2)-M_pre(1,2) , pre_pix(1)*M_pre(3,3)-M_pre(1,3) ;...
            pre_pix(2)*M_pre(3,1)-M_pre(2,1) , pre_pix(2)*M_pre(3,2)-M_pre(2,2) , pre_pix(2)*M_pre(3,3)-M_pre(2,3);...
            last_pix(1)*M_last(3,1)-M_last(1,1) , last_pix(1)*M_last(3,2)-M_last(1,2) , last_pix(1)*M_last(3,3)-M_last(1,3);...
            last_pix(2)*M_last(3,1)-M_last(2,1) , last_pix(2)*M_last(3,2)-M_last(2,2) , last_pix(2)*M_last(3,3)-M_last(2,3)];
        b = [M_pre(1,4) - pre_pix(1)*M_pre(3,4);...
            M_pre(2,4) - pre_pix(2)*M_pre(3,4);...
            M_last(1,4) - last_pix(1)*M_last(3,4);...
            M_pre(2,4) - last_pix(2)*M_last(3,4)];
    %% Get landmark value
        landmark_init_value = (A'*A)\(A'*b);
        landmark_init_value = gtsam.Point3(landmark_init_value);

end