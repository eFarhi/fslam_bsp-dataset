function [ compare ] = ibsp_ds17_UpdateNewFG( compare,newFactors,newValues)
%% ibsp_ds17_UpdateNewFG creates a new fg out of new factors

%% init
    new_factors = gtsam.NonlinearFactorGraph;
    new_values = gtsam.Values;
    new_fac_with_new_values = 0;
%% joining fg from inference & fg from planning
    for i = 1: length(compare.new_factors_indxIn1)
       new_factors.add(newFactors.at(compare.new_factors_indxIn1(i)));
       if ~isempty(newValues)
           factor = newFactors.at(compare.new_factors_indxIn1(i));
           keys = factor.keys;
           flag_new_fac_with_new_values = 0;
           for k = 0: keys.size -1
               try
                   new_values.insert(keys.at(k),newValues.at(keys.at(k)));
                   flag_new_fac_with_new_values = 1; 
               catch
               end
           end
           if flag_new_fac_with_new_values
              new_fac_with_new_values = new_fac_with_new_values + 1; 
           end
       end
    end
%% Stroing data
    compare.new_factors_from1 = new_factors;
    compare.new_values_from1 = new_values;
    compare.new_fac_with_new_values = new_fac_with_new_values;
end

