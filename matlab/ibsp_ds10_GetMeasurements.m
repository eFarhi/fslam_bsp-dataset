function [ Reading ] = ibsp_ds10_GetMeasurements( DS,pose_index,payload_name )
%IBSP_DS10_GETMEASUREMENTS returns measurements from payload_name at
%pose_index based on dataset DS

%% Get measurements from the desired payload
switch payload_name
    case 'Mono_Camera'
        Measurements = DS.Payloads.Mono_Camera.data{pose_index};
        Descriptor = DS.Payloads.Mono_Camera.desc{pose_index};
        %% Additional filtering for the measurements, using a pre-prossesed DA
%         DA_vec = full(DS.DA.Correspondence_mat(:,pose_index));
%         measurement_index = DA_vec > 0;
%         Measurements = Meas(:,DA_vec(measurement_index));
%         Descriptor = Desc(:,DA_vec(measurement_index));
        %% Saving the measurements 
        Reading = struct('Payload',payload_name,'Measurements',Measurements,'Descriptor',Descriptor,'Correspondence',[]);
        %% using pre-processed DA
        if ~isempty(DS.DA.Correspondence_mat)
           Reading.Correspondence =  DS.DA.Correspondence_mat(:,pose_index);
        end
    case 'Lidar'
        Measurements = [];%DS.Payloads.Lidar.data{pose_index};
        Descriptor = [];%DS.Payloads.Lidar.desc{pose_index};
        Reading = struct('Payload',payload_name);
    case 'Stereo_Camera'
        Measurements = [];%DS.Payloads.Stereo_Camera.data{pose_index};
        Descriptor = [];%DS.Payloads.Stereo_Camera.desc{pose_index};
        Reading = struct('Payload',payload_name);
    case 'IMU'
        Measurements = [];%DS.Payloads.IMU.data{pose_index};
        Descriptor = [];%DS.Payloads.IMU.desc{pose_index};
        Reading = struct('Payload',payload_name);
    otherwise
        warning('Payload Name is INCORRECT - no available measurements') 
         Reading = [];
end




