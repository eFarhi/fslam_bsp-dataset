function [FLAGS, Weights] = ibsp_ds01_PlanningConfigurationParams(MethodType)
%
%% Possible values of MethodType:
% - GBS
% - GBS-ML
% - GBS-L1
% - GBS-LogBarrier
% - GBS-NoUncertainty
% - GBS-IG
% - Discrete
% - Discrete-IG
% - Discrete-IG-Unfocused
% - iBSP

%% tbd
FLAGS.debug_flag = 0;
FLAGS.Control_type = 1; % 1: manual control
                       % 2: automatic control

FLAGS.pose_index_max = 1000;                       
                       
%% Simplified noise models for planning
FLAGS.Cov_process     = (15 * pi /180)^2; % noise on control angle
FLAGS.Cov_measurement = eye(3);
FLAGS.LandmarksDetectionMode   = 'Simplified'; % 'Simplified', 'Gaussian'

% %% Uncertainty bound
FLAGS.MaxPositionCovTrace = 3*50^2;
Weights.MaxPositionCovTrace = FLAGS.MaxPositionCovTrace;
FLAGS.covariance_drop_for_loop_closure =3*20^2;

FLAGS.go_to_goal_dist_th = 100; % go directly to goal if within this distance

Weights.Q_control_default = 0.1;
Weights.Q_control = Weights.Q_control_default; % initialization
Weights.Q_covariance = 1*eye(3);
Weights.Q_mean       = 1*eye(3);
Weights.alpha        = -1; % dummy initialization
Weights.alpha_lowerbound = 0.6;
Weights.require_loop_closure = 0;

%% DEFAULT 
FLAGS.completed_objectives = 0; % triggeres the robot to stop

Weights.PlanningWithoutControlEffort = 0; % when 1 planning is done *without* taking into account control effort
Weights.PlanningWithoutUncertainty   = 0; % when 1 planning is done *without* taking into account uncertainty

FLAGS.NumOfLookAheadSteps_max  = 5;
FLAGS.plan_only_with_curr_pose = 1;


FLAGS.UseLogBarrier = 0;
FLAGS.MaximumLikelihoodMeasurements   = 1;

FLAGS.Use_L1_norm = 0;

FLAGS.isam_optimization = 'Dogleg'; %supported values:  'GaussNewton' ; 'Dogleg'

FLAGS.odometry_calc_segments = 100;

FLAGS.PlanningAlgorithm = MethodType;

FLAGS.UseIG = false;

switch MethodType
    case 'GBS'        
    case 'GBS-ML'
        FLAGS.MaximumLikelihoodMeasurements = 1;   
    case 'GBS-L1'
        FLAGS.Use_L1_norm = 1;
        FLAGS.MaximumLikelihoodMeasurements = 1;   
    case 'GBS-LogBarrier'
        FLAGS.UseLogBarrier = 1;        
    case 'GBS-NoUncertainty'
        Weights.PlanningWithoutUncertainty   = 1;        
    case 'GBS-IG'        
        FLAGS.UseIG = true;
    case 'Discrete'
        FLAGS.PlanningAlgorithm = 'Discrete';
        % % comparison wth Kim13icra:
        FLAGS.MaximumLikelihoodMeasurements   = 1;
        FLAGS.PlanWithOnlyTerminalUncertainty = 1;        
        Weights.PlanningWithoutControlEffort = 1;
        FLAGS.UseLogBarrier = 0;
    case 'Discrete-IG'
        FLAGS.PlanningAlgorithm = 'Discrete-IG';
        FLAGS.UseIG = true;
        % % comparison wth Kim13icra:
        FLAGS.MaximumLikelihoodMeasurements   = 1;
        FLAGS.PlanWithOnlyTerminalUncertainty = 1;        
        Weights.PlanningWithoutControlEffort = 1;
        FLAGS.UseLogBarrier = 0;
    case 'Discrete-IG-Unfocused'
        FLAGS.PlanningAlgorithm = 'Discrete-IG-Unfocused';
        FLAGS.UseIG = true;
        % % comparison wth Kim13icra:
        FLAGS.MaximumLikelihoodMeasurements   = 1;
        FLAGS.PlanWithOnlyTerminalUncertainty = 1;        
        Weights.PlanningWithoutControlEffort = 1;
        FLAGS.UseLogBarrier = 0;        
    case 'iBSP'
        FLAGS.PlanningAlgorithm = 'iBSP';
        FLAGS.MaximumLikelihoodMeasurements = 1;

end

