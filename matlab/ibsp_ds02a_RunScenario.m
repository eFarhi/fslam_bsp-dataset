function ibsp_ds02a_RunScenario(DS, FLAGS, Weights, mean_pos_goal_all, manuever_delta_vec, p_to_saving, foldername)
%%
%

%% Post-Calc Storage
if exist([p_to_saving filesep foldername],'dir')
    error('Output folder already exists!')
end
mkdir(p_to_saving, foldername);
p_to_saving_results = [p_to_saving filesep foldername filesep];
%% Visualization configurations
fig_config = ibsp_ds05_PlotConfiguration(FLAGS,p_to_saving_results);

%% Load robot settings
RobotSettings = ibsp_ds03_RobotConfigurationParams(DS);
R_B_to_C = RobotSettings.R_B_to_C;
Pose_actual.Position_NED = RobotSettings.origin;
Pose_actual.R_G_to_C     = RobotSettings.R_G_to_C;

%% Create an iSAM2 object
if strcmp(FLAGS.isam_optimization,'') %'GaussNewton'
    isam_optimization = gtsam.ISAM2GaussNewtonParams;
else
    isam_optimization = gtsam.ISAM2DoglegParams;
end

% isam_optimization.setWildfireThreshold(0); % paper value => 0.001

isam_params = gtsam.ISAM2Params;
% isam_params.setRelinearizeThreshold(0); % default 0.1
isam_params.setOptimizationParams(isam_optimization);
isam_params.setFactorization('qr');
isam_params.setRelinearizeSkip(1); % default 10
isam_params.setEnableRelinearization(true); % default true
isam_params.setEnablePartialRelinearizationCheck(false); % default false

belief = gtsam.ISAM2(isam_params);

%% Set Prior & I.C

initial_pose = gtsam.Pose3(gtsam.Rot3(RobotSettings.origin.rot), gtsam.Point3(RobotSettings.origin.loc));

DA = ibsp_DataAssociation;
DA.Payloads.Mono_Camera.noise_model = RobotSettings.image_noise_model;
DA.Payloads.Mono_Camera.calib_mat = DS.Payloads.Mono_Camera.calib_mat;

newFactors.predicted = gtsam.NonlinearFactorGraph;
newFactors.new = gtsam.NonlinearFactorGraph;
newFactors.all = gtsam.NonlinearFactorGraph;

newValues.predicted  = gtsam.Values;
newValues.new  = gtsam.Values;
newValues.all  = gtsam.Values;

pose_curr_key = gtsam.symbol('x', 1);
factor = gtsam.PriorFactorPose3(pose_curr_key, initial_pose, gtsam.noiseModel.Diagonal.Sigmas(RobotSettings.initial_pose_sigma'));

newFactors.predicted.push_back(factor);
newValues.predicted.insert(pose_curr_key, initial_pose);

newFactors.all.push_back(factor);
newValues.all.insert(pose_curr_key, initial_pose);

keys.pose_keys_vec = pose_curr_key;
% keys.landmark_keys_vec = [];
%% first step Obs Model
pose_index = 1;
CurrentReading = ibsp_ds10_GetMeasurements(DS,pose_index,DS.OptionalPayloads.Mono_Camera);
[DA,newValues,newFactors,keys] = ibsp_ds08_DataAssociation(DA,CurrentReading,pose_index,newValues,newFactors,keys);

%% Initialize Manuever
result   = belief.update(newFactors.all, newValues.all);
estimate = belief.calculateEstimate();
DA.values = estimate;
Timing.compareFG_time = [];
Timing.infupdate = [];

%% Initialize LOG structure
OutputData.method{1}.method_name = 'iSAM';

pose_index = 1;
method_index = 1;
OutputData = ibsp_ds14_UpdateOutput(OutputData,method_index,belief,result,estimate,newValues.all,newFactors.all,pose_index,FLAGS,keys,manuever_delta_vec);

pose_index      = 2;
manuever_index  = 1;
fig_handle     = [];

InfUpdateOutput.results{3}.R_k1_k1 = [];
InfUpdateOutput.results{3}.d_k1_k1 = [];
InfUpdateOutput.results{3}.E = [];

%% Print Status

if fig_config.plot.plot_flag
    fig_handle = ibsp_ds13_Plot_data(fig_config,DA,pose_index,OutputData,[0],DS);
    mov(1) = getframe;
end
if ~FLAGS.upload == exist([p_to_saving_results filesep 'DetailedOutput'],'dir')
    error('DetailedOutput folder is missing or already exists!');
elseif ~exist([p_to_saving_results filesep 'DetailedOutput'],'dir')
    mkdir(p_to_saving_results, 'DetailedOutput');
end
%% Performing Passive Manuever
while ~FLAGS.completed_objectives
    pose_index
    %% Planning the next menuever
    if manuever_index > length(manuever_delta_vec)
        % commence planning
        [next_manuever,PlanningOutput,AdditionalOutput] = ibsp_ds06_Planning(DS,DA,keys,RobotSettings,FLAGS,pose_index,belief);
        if AdditionalOutput.manuever_length == 0
            % if there isn't next step, terminate
            FLAGS.completed_objectives = true;
            continue
        else
            % MPC
            manuever_delta_vec{end+1} = next_manuever{1};
        end
    end
    manuever_delta = manuever_delta_vec{manuever_index};
    %% Move the Robot
    pose_prev_key = pose_curr_key;
    pose_curr_key = gtsam.symbol('x', pose_index);
    keys.pose_keys_vec = [keys.pose_keys_vec; pose_curr_key];
    
    %% Motion Model
    % Create Factor Graph and Values to hold new measurements
    newFactors.predicted = gtsam.NonlinearFactorGraph;
    newFactors.new = gtsam.NonlinearFactorGraph;
    newFactors.all = gtsam.NonlinearFactorGraph;
    
    newValues.predicted  = gtsam.Values;
    newValues.new  = gtsam.Values;
    newValues.all  = gtsam.Values;
    
    [odometry] = ibsp_ds04_GetOdometry(DS,RobotSettings,pose_index,manuever_delta);
    
    odom_fac = gtsam.BetweenFactorPose3(pose_prev_key, pose_curr_key, odometry, RobotSettings.odometry_noise_model);
    newFactors.predicted.push_back(odom_fac);
    newFactors.all.push_back(odom_fac);
    pose_predict = estimate.at(pose_prev_key).compose(odometry);
    newValues.predicted.insert(pose_curr_key, pose_predict);
    newValues.all.insert(pose_curr_key, pose_predict);
    
    %% Obs Model
    %     CurrentReading = ibsp_ds10_GetMeasurements(DS,pose_index,DS.OptionalPayloads.Mono_Camera);
    Measurements = DS.Payloads.Mono_Camera.data{pose_index};
    Descriptor = DS.Payloads.Mono_Camera.desc{pose_index};
    CurrentReading = struct('Payload',DS.OptionalPayloads.Mono_Camera,'Measurements',Measurements,'Descriptor',Descriptor,'Correspondence',[]);
    CurrentReading.Correspondence =  DS.DA.Correspondence_mat(:,pose_index);
    [DA,newValues,newFactors,keys,DA_output] = ibsp_ds08_DataAssociation(DA,CurrentReading,pose_index,newValues,newFactors,keys,RobotSettings);
    
    count.existing_landmark_num(pose_index) = DA_output.existing_landmarks_num;
    count.new_landmarks(pose_index) = DA_output.new_landmarks_num;
    count.new_fac_of_new_landmarks(pose_index) = DA_output.fac_of_new_landmarks;
    count.fac_k1_k1_size(pose_index) = newFactors.all.size;
 
     %% inference Update for timing
    if newFactors.predicted.size() > 0
       %% updating inference
        belief.update(newFactors.all,newValues.all);
        for i = 1:5
            result = belief.update();
        end
        estimate = belief.calculateBestEstimate();
        DA.values = estimate;
    end
    
    %% Save 2 Output
    
    method_index = 1;
    OutputData = ibsp_ds14_UpdateOutput(OutputData,method_index,belief, result,estimate,newValues.all,newFactors.all,pose_index,FLAGS,keys,manuever_delta_vec);
       
    % Update figure
    if fig_config.plot.plot_flag
        fig_handle = ibsp_ds13_Plot_data(fig_config,DA,pose_index,OutputData,[0],DS,fig_handle);
        %         mov(pose_index+1) = getframe; % for creating a movie
    end
    %% Prep 4 Next Step
    manuever_index = manuever_index +1;
    pose_index = pose_index + 1;
%     save([p_to_saving_results 'DetailedOutput/Counting_' num2str(pose_index-1)], 'count');
    % save DA matrix
       temp_DA_payloads = DA.Payloads;
       save([p_to_saving filesep foldername filesep 'DetailedOutput/DA',num2str(pose_index -1)],'temp_DA_payloads');
    
end

%% save output file
OutputData.last_pose_index = pose_index-1;
% save([p_to_saving_results 'OutputData'], 'OutputData');


end


