function [odometry_measured] = ibsp_ds04_GetOdometry(DS,RobotSettings,pose_index,manuever_delta)


%% get odom straight from gt 
% GT_global_k1 = gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{pose_index}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{pose_index}(1:3,4)));
% if pose_index > 1
%     former_pose_index = pose_index -1;
%     GT_global_k =  gtsam.Pose3(gtsam.Rot3(DS.GroundTruth.data{former_pose_index}(1:3,1:3)),gtsam.Point3(DS.GroundTruth.data{former_pose_index}(1:3,4)));
% else
%     GT_global_k = gtsam.Pose3(gtsam.Rot3(eye(3)),gtsam.Point3(zeros(3,1)));
% end

% odometry_truth = GT_global_k.between(GT_global_k1); 

%% get odom as curropted action command
odometry_sigma = RobotSettings.odometry_sigma;

noise = mvnrnd(zeros(size(odometry_sigma)), odometry_sigma.^2)';

odometry_measured = manuever_delta.retract(noise);

%% ########## Scaffolding ############
% odometry_measured = manuever_delta;
% ###################################
